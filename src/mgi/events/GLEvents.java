package mgi.events;

import java.util.Hashtable;
import java.util.Vector;

import mgi.util.Utils;

public class GLEvents {

	public static final String EVENT_GPS_POSITION_UPDATED = "gpsUpdated";
	public static final String EVENT_DATABASE_LOAD_FAILURE = "dbFailed";
	public static final String EVENT_DATABASE_CREATED = "dbCreated";
	public static final String EVENT_DATABASE_OPENED = "dbOpened";
	public static final String EVENT_DATABASE_READY = "dbReady";
	public static final String EVENT_GPS_PROVIDER_STATUS_CHANGED = "gpsProviderChanged";
	public static final String EVENT_APP_DEACTIVATED = "appGoneToBackground";
	public static final String EVENT_APP_ACTIVATED = "appGoneToForeground";
	
	private static Hashtable events = new Hashtable();

	public static void addEvent(String eventName) {
		if (!events.containsKey(eventName)) {
			events.put(eventName, new Vector());
		}
	}

	public static boolean eventExists(String eventName) {
		return events.containsKey(eventName);
	}

	public static Vector getListeners(String eventName) {
		if (eventExists(eventName)) {
			return (Vector) events.get(eventName);
		}

		return new Vector();
	}

	public static void triggerEvent(String eventName) {
		if(eventName == null){
			return;
		}
		
		if (!eventExists(eventName)) {
			addEvent(eventName);
		}

//		Utils.log("Event triggered: " + eventName);
		notifyListeners(eventName, null);
	}

	public static void addEventListener(String eventName,
			GLEventListener listener) {
		if (!eventExists(eventName)) {
			addEvent(eventName);
		}

		Vector listeners = getListeners(eventName);
		listeners.addElement(listener);
		events.put(eventName, listeners);
	}
	
	
	public static void removeEventListener(String eventName, GLEventListener listener){
		if(!eventExists(eventName)){
			return;
		}
		
		Vector listeners = getListeners(eventName);
		listeners.removeElement(listener);
	}

	private static void notifyListeners(String eventName, Hashtable dict) {
		Vector listeners = getListeners(eventName);
		int numberOfListeners = listeners.size();
//		Utils.log("Notifying " + numberOfListeners
//				+ " listener(s) of: " + eventName);

		for (int i = 0; i < numberOfListeners; i++) {
			GLEventListener listener = (GLEventListener) listeners.elementAt(i);
			listener.eventOccurred(eventName, dict);
		}
	}

	public static void triggerEvent(String eventName, Hashtable dict) {
		if (!eventExists(eventName)) {
			addEvent(eventName);
		}

//		Utils.log("Event triggered: " + eventName);
		notifyListeners(eventName, dict);
	}

}
