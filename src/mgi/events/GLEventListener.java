package mgi.events;

import java.util.Hashtable;

public interface GLEventListener {
	public void eventOccurred(String eventName, Hashtable data);
}