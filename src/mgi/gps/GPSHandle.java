package mgi.gps;

import java.util.Hashtable;

import javax.microedition.location.Coordinates;
import javax.microedition.location.Criteria;
import javax.microedition.location.Location;
import javax.microedition.location.LocationException;
import javax.microedition.location.LocationListener;
import javax.microedition.location.LocationProvider;
import javax.microedition.location.QualifiedCoordinates;

import mgi.events.GLEventListener;
import mgi.events.GLEvents;
import mgi.events.GLPrerequisites;
import mgi.util.Utils;
import net.rim.device.api.gps.BlackBerryCriteria;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.gps.BlackBerryLocationProvider;
import net.rim.device.api.gps.GPSInfo;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

/**
 * This class manages the GPS point changes for the application. Call
 * setupGPSListener to begin listening in for changes in location.
 * 
 * 
 * Sample points: lat,lon 18.00281515641136, -76.74962997436523 - mgi
 * 18.01824227384845, -76.75658226013184 - jc
 * 
 * @author JMWright
 * 
 */
public class GPSHandle implements LocationListener, GLEventListener {

	/**
	 * Ensures that GPS is setup first
	 */
	private static boolean setupComplete = false;

	private static long timeOfLastFix = System.currentTimeMillis();

	private static long timeOfFirstFix = 0;

	private static long creationTime = System.currentTimeMillis();

	/**
	 * Private constructor, use "getHandle" to get the instance of this class.
	 */
	private GPSHandle() {
		// Listen for when the app goes into the background
		GLEvents.addEventListener(GLEvents.EVENT_APP_ACTIVATED, this);
		GLEvents.addEventListener(GLEvents.EVENT_APP_DEACTIVATED, this);
	}

	protected final float horizontalAccuracy = 0;
	protected final float verticalAccuracy = 0;

	private boolean supported = false;

	/**
	 * Private gps handle .. one for application
	 */
	private static final GPSHandle gpsHandle = new GPSHandle();

	private int gpsMode = -1;

	public int getGpsMode() {
		return gpsMode;
	}

	private void setGpsMode(int gpsMode) {
		this.gpsMode = gpsMode;
	}

	protected static final String ERROR_NO_GPS_AVAILABLE = "No GPS is available";

	/**
	 * Returns an instance to this class
	 * 
	 * @return
	 */
	public static GPSHandle getHandle() {
		return gpsHandle;
	}

	protected BlackBerryLocationProvider provider = null;

	protected BlackBerryCriteria criteria = null;

	/**
	 * The current location
	 */
	protected BlackBerryLocation currentLocation = null;
	
	protected int locationMethod = 0;

	private String dataSource;

	private int state;

	private int status;

	/**
	 * Gets the last Location received
	 * 
	 * @return
	 */
	public static BlackBerryLocation getLocation() {
		return gpsHandle.currentLocation;
	}

	/**
	 * Get a location, but willing to wait at least timeout seconds
	 * 
	 * @param timeout - the time out in seconds
	 * @return
	 * @throws LocationException
	 * @throws InterruptedException
	 */
	public static BlackBerryLocation getLocation(int timeout) throws LocationException, InterruptedException {
		if (gpsHandle.provider != null) {
			if (isValid()) {
				return gpsHandle.currentLocation;
			}
			BlackBerryLocationProvider provider = gpsHandle.provider;
			BlackBerryLocation location = (BlackBerryLocation) provider.getLocation(timeout);
			if (isValid(location)) {
				Utils.log("GPS: Got back a valid position after waiting " + timeout + "s");
				gpsHandle.locationUpdated(provider, location);
				return location;
			}
			Utils.log("GPS: Got back an INVALID position after waiting " + timeout + "s");
			if(location != null){
				Utils.log("Valid? " + location.isValid());
				if(location.getQualifiedCoordinates() != null){
					QualifiedCoordinates qc = location.getQualifiedCoordinates();
					double lat = qc.getLatitude();
					double lon = qc.getLongitude();
					Utils.log("GPS: Qualified coords are: " + lat + "," + lon);
				} else {
					Utils.log("GPS: Point has no qualified coordinates");
				}
			} else {
				Utils.log("GPS: Location was NULL");
			}
		}
		return null;
	}

	public static BlackBerryLocation getLastKnownLocation() {
		if (gpsHandle.provider != null) {
			BlackBerryLocation location = GPSHandle.getLocation();
			if (isValid(location)) {
				return location;
			}
		}
		return null;
	}

	public static boolean isValid(double lat, double lon) {
		return lat >= 17 && lat <= 19 && lon > -79 && lon < -75;
	}

	/**
	 * Updates the location variable stored internally
	 * 
	 * @param Location
	 *            l - the location
	 */
	protected boolean setLocation(BlackBerryLocation l) {
		if (l == null) {
			return false;
		}

		// we might not be able to get qualified coords
		if (l.getQualifiedCoordinates() == null) {
			return false;
		}

		double lat = l.getQualifiedCoordinates().getLatitude();
		double lon = l.getQualifiedCoordinates().getLongitude();

		if (isValid(lat, lon)) {
			GPSHandle.timeOfLastFix = System.currentTimeMillis();
			if (GPSHandle.timeOfFirstFix == 0) {
				GPSHandle.timeOfFirstFix = System.currentTimeMillis() - creationTime;
			}
			Utils.log("GPS: New location received: " + lat + "," + lon);

			this.currentLocation = l;
			this.locationMethod = l.getLocationMethod();
			if (!GLPrerequisites.isPrerequisiteComplete(GLPrerequisites.PREREQ_GPS_FIX_ACQUIRED)) {
				GLPrerequisites.prerequisiteComplete(GLPrerequisites.PREREQ_GPS_FIX_ACQUIRED);
			}
			// Updates the coordinates and recalculate the distances if
			// necessary
			GLEvents.triggerEvent(GLEvents.EVENT_GPS_POSITION_UPDATED);
			return true;

		} else {
			Utils.err("GPS: Discarded invalid point: " + lat + ", " + lon);
			return false;
		}
	}

	private static void setupCriteria(int gpsMode, BlackBerryCriteria criteria) {
		if (gpsMode == -1) {
			Utils.warn("GPS: Got invalid GPS mode, cannot setup criteria");
			return;
		}

		gpsHandle.gpsMode = gpsMode;

		switch (gpsMode) {

		case GPSInfo.GPS_MODE_CELLSITE:
			Utils.log("GPS: Setting up CELLSITE criteria");
			break;
		case GPSInfo.GPS_MODE_ASSIST:
			Utils.log("GPS: Setting up ASSIST criteria");
			criteria.setHorizontalAccuracy(200);
			criteria.setVerticalAccuracy(200);
			criteria.setPreferredPowerConsumption(Criteria.POWER_USAGE_MEDIUM);
			criteria.setSubsequentMode(GPSInfo.GPS_MODE_AUTONOMOUS);
			criteria.setFailoverMode(GPSInfo.GPS_MODE_AUTONOMOUS, 1, 30);
			break;

		case GPSInfo.GPS_MODE_AUTONOMOUS:
			Utils.log("GPS: Setting up AUTONOMOUS criteria");
			gpsHandle.setSupported(true);
			criteria.setPreferredPowerConsumption(0);
			criteria.setCostAllowed(false);
			criteria.setFailoverMode(GPSInfo.GPS_MODE_ASSIST, 5, 60000);
			break;
		}

	}

	public static boolean isGPSCellSiteSupported() {
		return GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE);
	}

	public static boolean isGPSInternalDeviceSupported() {
		return GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS);
	}
	
	public static void enableGPS(){
		Utils.log("Enabling GPS");
		setupGPSListener();
	}
	
	public static void disableGPS(){
		Utils.log("Disabling GPS");
		if(gpsHandle == null){
			return;
		}
		
		if(gpsHandle.provider != null){
			gpsHandle.provider.setLocationListener(null, -1, -1, -1);
		}
		setupComplete = false;
	}
	

	/**
	 * Starts a thread that registers a listener for GPS position updates
	 */
	public static void setupGPSListener() {
		if (setupComplete) {
			Utils.warn("GPS: Setup already done, no need to do this again");
			return;
		}

		int gpsMode = -1;
		gpsHandle.criteria = new BlackBerryCriteria();
		if (GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_CELLSITE)) { 
			Utils.log("GPS: Using cell site mode");
			gpsMode = GPSInfo.GPS_MODE_CELLSITE;
		} else if (GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_ASSIST)) {
			// set the fail over mode to be cell site mode
			// gpsHandle.criteria.setFailoverMode(GPSInfo.GPS_MODE_CELLSITE, 3,
			// 40);
			Utils.log("GPS: Using assisted mode");
			gpsMode = GPSInfo.GPS_MODE_ASSIST;
		} else if (GPSInfo.isGPSModeAvailable(GPSInfo.GPS_MODE_AUTONOMOUS)) {
			Utils.log("GPS: Using internal device");
			gpsMode = GPSInfo.GPS_MODE_AUTONOMOUS;
		}

		if (gpsMode != -1) {
			gpsHandle.setSupported(true);

			Utils.log("GPS: Setting up listener");
			GLPrerequisites.prerequisiteStarted(GLPrerequisites.PREREQ_GPS_LISTENER_SETUP);
			GLPrerequisites.prerequisiteStarted(GLPrerequisites.PREREQ_GPS_FIX_ACQUIRED);

			try {
				gpsHandle.criteria.setMode(gpsMode);
				// setupCriteria(gpsMode, gpsHandle.criteria);
				gpsHandle.provider = (BlackBerryLocationProvider) LocationProvider.getInstance(gpsHandle.criteria);
				gpsHandle.provider.setLocationListener(gpsHandle, 30, -1, -1);
				Utils.log("GPS: Listener setup");
				GLPrerequisites.prerequisiteComplete(GLPrerequisites.PREREQ_GPS_LISTENER_SETUP);
				setupComplete = true;
			} catch (LocationException e) {
				GLPrerequisites.prerequisiteFailed(GLPrerequisites.PREREQ_GPS_LISTENER_SETUP);
				GLPrerequisites.prerequisiteFailed(GLPrerequisites.PREREQ_GPS_FIX_ACQUIRED);
				Dialog.alert(e.getMessage());
				e.printStackTrace();
			}
		} else {
			Utils.err("GPS: No GPS support available");
			gpsHandle.setSupported(false);
			Utils.err("GPS: No GPS available");
			GLPrerequisites.prerequisiteFailed(GLPrerequisites.PREREQ_GPS_LISTENER_SETUP);
			GLPrerequisites.prerequisiteFailed(GLPrerequisites.PREREQ_GPS_FIX_ACQUIRED);
		}
	}

	/**
	 * This method is called each time a new fix is received. Override to
	 * provide additional functionality. Retain the call to the super class as
	 * this updates the location variable stored internally.
	 */
	public void locationUpdated(LocationProvider provider, Location location) {
		if (provider != null) {

			BlackBerryLocationProvider bbProvider = (BlackBerryLocationProvider) provider;

			setState(bbProvider.getState());
			BlackBerryLocation bbLocation = (BlackBerryLocation) location;

			int locationMethod = bbLocation.getLocationMethod();
			if ((locationMethod & Location.MTA_ASSISTED) > 0) {
				Utils.log("GPS: Provider used Network Assist to get GPS point");
			}

			if ((locationMethod & Location.MTE_CELLID) > 0) {
				Utils.log("GPS: Provider used Cell ID to get GPS point");
			}
		}

		if (location.isValid() && isValid(location)) {
			BlackBerryLocation bbLocation = (BlackBerryLocation) location;
			setStatus(bbLocation.getStatus());
			String dataSource = bbLocation.getDataSource() == 1 ? "Internal" : "Unknown";
			setDataSource(dataSource);

			setLocation(bbLocation);
		} else {
			Utils.warn("GPS: Discarding invalid location: isValid? " + location.isValid());
			if (location.getQualifiedCoordinates() != null) {
				QualifiedCoordinates qc = location.getQualifiedCoordinates();
				Utils.warn("GPS: These coords are invalid: " + qc.getLatitude() + "," + qc.getLongitude());
			}
		}
	}

	private void setStatus(int status) {
		String msg = "Unknown";
		if (status == BlackBerryLocation.FAILOVER_MODE_ON) {
			msg = "Fail over mode on";
		} else if (status == BlackBerryLocation.SUBSEQUENT_MODE_ON) {
			msg = "Subsequent mode on";
		} else if (status == BlackBerryLocation.GPS_ERROR) {
			msg = "Error";
		} else if (status == BlackBerryLocation.GPS_FIX_UNAVAILABLE) {
			msg = "Fix unavailable";
		}
		Utils.log("GPS: Status: " + msg);
		gpsHandle.status = status;
	}

	/**
	 * Returns the status of the valid location we got
	 * 
	 * @return
	 */
	public static int getStatus() {
		return gpsHandle.status;
	}

	public static boolean isFailOverModeOn() {
		return gpsHandle.status == BlackBerryLocation.FAILOVER_MODE_ON;
	}

	public static boolean isSubsequentModeOn() {
		return gpsHandle.status == BlackBerryLocation.SUBSEQUENT_MODE_ON;
	}

	/**
	 * Checks if the last fix used cell site...
	 * @return
	 */
	public static boolean isUsingCellSite() {
		if(isGPSSupported() && gpsHandle != null){
			int locationMethod = gpsHandle.locationMethod;
			return (locationMethod & Location.MTE_CELLID) > 0; 
		}
		return false;
	}

	public static boolean isUsingInternalGPS(){
		if(isGPSSupported() && gpsHandle != null){
			int locationMethod = gpsHandle.locationMethod;
			return (locationMethod & Location.MTE_SATELLITE) > 0; 
		}
		return false;
	}

	private static void setDataSource(String dataSource) {
		Utils.log("GPS: Datasource is " + dataSource);
		gpsHandle.dataSource = dataSource;
	}

	public static String getDataSource() {
		return gpsHandle.dataSource;
	}

	private static void setState(int state) {
		String stateName = "Unknown";
		if (state == LocationProvider.AVAILABLE) {
			stateName = "Available";
		} else if (state == LocationProvider.TEMPORARILY_UNAVAILABLE) {
			stateName = "Temp Unavailable";
		} else if (state == LocationProvider.OUT_OF_SERVICE) {
			stateName = "Out of service";
		}
		Utils.log("GPS: Provider state changed to: " + stateName);
		gpsHandle.state = state;
	}

	public static int getState() {
		return gpsHandle.state;
	}

	public static boolean isAvailable() {
		return gpsHandle.state == LocationProvider.AVAILABLE;
	}

	public static boolean isOutOfService() {
		return gpsHandle.state == LocationProvider.OUT_OF_SERVICE;
	}

	public static boolean isTemporarilyOutOfService() {
		return gpsHandle.state == LocationProvider.TEMPORARILY_UNAVAILABLE;
	}

	public void providerStateChanged(LocationProvider provider, int newState) {
		GPSHandle.setState(newState);
		switch (newState) {
		case LocationProvider.AVAILABLE:
			Utils.log("GPS: Provider is available");
			break;
		case LocationProvider.OUT_OF_SERVICE:
			Utils.err("GPS: Provider is OUT OF SERVICE");
			break;
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
			Utils.warn("GPS: Provider is temporarily out of service");
		}

		if (newState == LocationProvider.AVAILABLE && provider != null && provider != gpsHandle.provider) {
			gpsHandle.provider = (BlackBerryLocationProvider) provider;
		}

		GLEvents.triggerEvent(GLEvents.EVENT_GPS_PROVIDER_STATUS_CHANGED);
	}

	public static boolean isGPSSupported() {
		return gpsHandle.isSupported();
	}

	protected boolean isSupported() {
		return supported;
	}

	/**
	 * Returns true if a current location is valid
	 * 
	 * @param l
	 * @return
	 */
	public static boolean isValid(Location l) {
		if (l == null) {
			return false;
		}

		if (l.getQualifiedCoordinates() == null) {
			return false;
		}

		double lat = l.getQualifiedCoordinates().getLatitude();
		double lon = l.getQualifiedCoordinates().getLongitude();
		return isValid(lat, lon);
	}

	/**
	 * Returns true if the current point stored is valid
	 * 
	 * @return
	 */
	public static boolean isValid() {
		return isValid(GPSHandle.getLocation());
	}

	protected void setSupported(boolean supported) {
		this.supported = supported;
	}

	public static double getLatitude() {
		Location l = gpsHandle.currentLocation;
		if (l != null) {
			return l.getQualifiedCoordinates().getLatitude();
		}
		return -1;
	}

	public static double getLongitude() {
		Location l = gpsHandle.currentLocation;
		if (l != null) {
			return l.getQualifiedCoordinates().getLongitude();
		}
		return -1;
	}

	public static String getDistanceAsString(double distance) {
		distance = Math.ceil(distance);
		String distanceFromUser = "";
		String unit = "m";
		if (distance >= 1000) {
			unit = "km";
			distance /= 1000;

			if (distance > 500) {
				distanceFromUser = " >500 " + unit;
			} else {
				distanceFromUser = ((int) distance) + " " + unit;
			}
		}

		if (distance < 0 || distance > 500) {
			distanceFromUser = " ... ";
		}
		return distanceFromUser;
	}

	public BlackBerryLocationProvider getProvider() {
		return provider;
	}

	protected void setProvider(BlackBerryLocationProvider provider) {
		this.provider = provider;
	}

	public BlackBerryCriteria getCriteria() {
		return criteria;
	}

	protected void setCriteria(BlackBerryCriteria criteria) {
		this.criteria = criteria;
	}

	public static long getTimeOfLastFix() {
		return timeOfLastFix;
	}

	public static void setTimeOfLastFix(long timeOfLastFix) {
		GPSHandle.timeOfLastFix = timeOfLastFix;
	}

	public static boolean timeOfLastFixLessThan(long milliseconds) {
		return (System.currentTimeMillis() - timeOfLastFix) < milliseconds;
	}

	public static boolean timeOfLastFixGreaterThan(long milliseconds) {
		Utils.log("GPS: Time of last fix was: " + timeOfLastFix);
		long now = System.currentTimeMillis();
		long diff = now - timeOfLastFix;
		Utils.log("GPS: Current time is: " + now);
		Utils.log("GPS: Time diff is: " + diff);
		Utils.log("Comparing to: " + milliseconds);
		return diff > milliseconds;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(BlackBerryLocation currentLocation) {
		this.currentLocation = currentLocation;
	}

	public static float getVerticalaccuracy() {
		return GPSHandle.getHandle().verticalAccuracy;
	}

	public static float getHorizontalaccuracy() {
		return GPSHandle.getHandle().horizontalAccuracy;
	}

	public static long getTimeOfFirstFix() {
		return timeOfFirstFix;
	}

	public void eventOccurred(String eventName, Hashtable data) {
		if(eventName == GLEvents.EVENT_APP_DEACTIVATED){
			disableGPS();
		} else if(eventName == GLEvents.EVENT_APP_ACTIVATED){
			enableGPS();
		}
	}
	
	/**
	 * Does a simlpe minus and converts the difference in degs to metres 
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return
	 */
	public static double simpleDistance(double lat1, double lon1, double lat2, double lon2){
		double latDiff = lat2-lat1;
		double lonDiff = lon2-lon1;
		double deg = Math.sqrt((latDiff*latDiff)+(lonDiff*lonDiff));
		double dist = 6378137.0 * Math.PI * deg / 180;
		return dist;
	}
	
	/**
	 * Removes the current location
	 */
	public static void clearLocation(){
		gpsHandle.currentLocation = null;
	}
	
	/**
	 * Returns true if a fix has ever been obtained
	 * @return
	 */
	public static boolean isFirstFixObtained(){
		return GPSHandle.timeOfFirstFix > 0;
	}
	
}
