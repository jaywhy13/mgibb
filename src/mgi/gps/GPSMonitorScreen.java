package mgi.gps;

import java.util.Hashtable;

import javax.microedition.location.Location;

import mgi.events.GLEventListener;
import mgi.events.GLEvents;
import mgi.maps.Google;
import mgi.net.GLNetwork;
import mgi.net.GLNetworkAction;
import mgi.net.GLNetworkManager;
import mgi.util.Utils;
import net.rim.device.api.gps.BlackBerryLocation;
import net.rim.device.api.gps.GPSInfo;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.decor.Border;
import de.enough.glaze.ui.component.BitmapField;
import de.enough.glaze.ui.component.LabelField;
import de.enough.glaze.ui.container.MainScreen;
import de.enough.glaze.ui.container.VerticalFieldManager;

public class GPSMonitorScreen extends MainScreen implements GLEventListener {

	LabelField gpsCoordField;
	LabelField providerInfoField;
	LabelField fixInfoField;
	BitmapField staticMapField;

	VerticalFieldManager vfm;
	boolean ready = false;
	private long timeOfLastFix = 0;
	private int numberOfFixes = 0;
	private double lat = 0;
	private double lon = 0;

	public GPSMonitorScreen() {
		setTitle("GPS Monitor");

		gpsCoordField = new LabelField("...", LabelField.FIELD_HCENTER);
		providerInfoField = new LabelField("...", LabelField.FIELD_HCENTER);
		staticMapField = new BitmapField();
		fixInfoField = new LabelField("No fixes yet");

		vfm = new VerticalFieldManager();

		add(vfm);
		vfm.add(staticMapField);
		vfm.add(gpsCoordField);
		vfm.add(fixInfoField);
		vfm.add(providerInfoField);
		GLEvents.addEventListener(GLEvents.EVENT_GPS_POSITION_UPDATED, this);
		GLEvents.addEventListener(GLEvents.EVENT_GPS_PROVIDER_STATUS_CHANGED, this);
		ready = true;
		
		checkGPSStatus();
	}


	public void updateImage(double lat, double lon) {
		String url = Google.getStaticMapUrl("*", lat, lon, 18, 300, 300);
		loadImage(staticMapField, url);
	}


	public void updateCoord(double lat, double lon) {
		if(lat == this.lat && lon == this.lon){
			return;
		}
		
		this.lat = lat;
		this.lon = lon;
		numberOfFixes++;
		
		// Update the coordinate
		final String message =  lat + "," + lon;
		Utils.invokeLater(new Runnable() {
			public void run() {
				gpsCoordField.setText(message);
			}
		});
		updateImage(lat, lon);

		// update the timestamp
		if (timeOfLastFix == 0) {
			timeOfLastFix = System.currentTimeMillis();
		} else {
			long diff = System.currentTimeMillis() - timeOfLastFix;
			updateTimeElapsed(diff);
			timeOfLastFix = System.currentTimeMillis();
		}
	}

	/**
	 * Updates the time elapsed count between last fix and now
	 * @param ms
	 */
	public void updateTimeElapsed(final double ms) {
		Utils.invokeLater(new Runnable() {
			public void run() {
				fixInfoField.setText("Fix #" + numberOfFixes + " in " + (ms / 1000) + "s");
			}
		});
	}

	public void updateProviderStatus(final String message) {
		
		Utils.invokeLater(new Runnable() {
			public void run() {
				providerInfoField.setText("Provider: " + message);
			}
		});
	}

	public void checkGPSStatus() {
		if(GPSHandle.isValid()){
			updateCoord(GPSHandle.getLatitude(), GPSHandle.getLongitude());
		}
	}
	
	public void checkProviderStatus(){
		// Provider type
		int providerType = GPSHandle.getHandle().getProvider().getProviderType();
		String providerTypeStr = "Unknown";
		if (providerType != -1) {
			switch (providerType) {
			case GPSInfo.GPS_MODE_ASSIST:
				providerTypeStr = "Assist";
				break;
			case GPSInfo.GPS_MODE_AUTONOMOUS:
				providerTypeStr = "Autonomous";
				break;
			case GPSInfo.GPS_MODE_CELLSITE:
				providerTypeStr = "Cellsite";
			case GPSInfo.GPS_MODE_NONE:
				providerTypeStr = "No mode";
			}
		}


		String gpsValidity = GPSHandle.isValid() ? "Valid" : "Invalid";
		String dataSource = "Unknown";
		if (GPSHandle.isValid()) {
			BlackBerryLocation location = (BlackBerryLocation) GPSHandle.getLocation();
			dataSource = location.getDataSource() == 1 ? "Internal" : "Unknown";
		}
		
		final String providerStatus = "Provider " + providerTypeStr + " is " + gpsValidity + " using " + dataSource;
		Utils.invokeLater(new Runnable(){
			public void run(){
				providerInfoField.setText(providerStatus);
			}
		});
	}

	public void eventOccurred(String eventName, Hashtable data) {
		if(eventName == GLEvents.EVENT_GPS_POSITION_UPDATED){
			checkGPSStatus();
		}
		checkProviderStatus();
	}

	public void loadImage(final BitmapField field, final String url) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				GLNetwork network = GLNetworkManager.getNetworkManager();
				if (network.canConnect()) {
					Utils.log("Loading image from url: " + url);
					GLNetworkAction action = new GLNetworkAction() {
						public void success(Object data, int status) {
							final Bitmap bitmap = getImage();
							if (bitmap != null) {
								UiApplication.getUiApplication().invokeLater(new Runnable() {
									public void run() {
										field.setBitmap(bitmap);
									}
								});
							}
						}
					};
					network.get(url, action);
				}
			}
		});
		t.start();
	}

}
