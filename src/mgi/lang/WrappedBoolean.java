package mgi.lang;

public class WrappedBoolean {
	private boolean value = false;

	public boolean value() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}
	
}
