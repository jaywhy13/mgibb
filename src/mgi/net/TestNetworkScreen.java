package mgi.net;

import mgi.util.StoppableRunnable;
import mgi.util.Utils;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import de.enough.glaze.ui.component.ButtonField;
import de.enough.glaze.ui.component.GaugeField;
import de.enough.glaze.ui.component.LabelField;
import de.enough.glaze.ui.component.SeparatorField;
import de.enough.glaze.ui.container.MainScreen;

public class TestNetworkScreen extends MainScreen implements
		FieldChangeListener {

	GaugeField progressBar;
	LabelField failuresLabel;
	LabelField successLabel;
	
	LabelField apnField;
	LabelField connFacField;

	ButtonField startButton;
	ButtonField cancelButton;

	LabelField responseTimeLabel;

	LabelField availableTransports;

	ObjectChoiceField numRequestField;

	Thread networkTest;

	private int failures = 0;
	private int successes = 0;
	private long totalResponseTime = 0;
	private int test = 0;

	public TestNetworkScreen() {
		// setup the components...
		progressBar = new GaugeField();

		successLabel = new LabelField("Success: 0");
		failuresLabel = new LabelField("Failed: 0");

		startButton = new ButtonField("Start");
		cancelButton = new ButtonField("Cancel");

		responseTimeLabel = new LabelField("Avg Resp Time: ");

		availableTransports = new LabelField("Transports: ");
		
//		connFacField = new LabelField("Method: " + (GLNetworkNavigate.isUseConnectionFactory() ? " Connection Factory" : "Connection Strings" ) );
		
//		apnField = new LabelField("APN: " + GLNetworkNavigate.getApn());

		startButton.setChangeListener(this);
		cancelButton.setChangeListener(this);

		numRequestField = new ObjectChoiceField("# of Requests", new String[] {
				"20", "40", "50", "70", "100", "150" });

		// Add the stuff
		add(progressBar);
		add(new SeparatorField());

		add(successLabel);
		add(failuresLabel);
		add(responseTimeLabel);
		add(availableTransports);
		
		add(availableTransports);
//		add(apnField);

		add(numRequestField);

		HorizontalFieldManager hfm = new HorizontalFieldManager(
				Field.FIELD_HCENTER);
		hfm.add(startButton);
		hfm.add(cancelButton);
		add(hfm);

	}

	public int getNumberOfRequests() {
		int index = numRequestField.getSelectedIndex();
		String num = numRequestField.getChoice(index).toString();
		return Integer.valueOf(num).intValue();
	}

	private void updateTransports(String transports) {
		availableTransports.setText("Transports: " + transports);
	}

	private void updateTransportList() {
		Utils.invokeLater(new Runnable() {
			public void run() {
				GLNetworkNavigate network = (GLNetworkNavigate) GLNetworkManager
						.getNetworkManager();
				String transports = Utils.join(
						network.getAvailableConnectionTypes(), ",");
				updateTransports(transports);
			}
		});
	}

	public void startTests() {
		stopTests();

		Utils.startThread(new StoppableRunnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(2000);
						updateTransportList();
					} catch (InterruptedException e) {
					}
				}
			}
		});

		networkTest = new Thread(new Runnable() {
			public void run() {
				// Init the progress bar
				Utils.invokeLater(new Runnable() {
					public void run() {
						_initProgressBar();
					}
				});

				test = 0;
				int numOfRequests = getNumberOfRequests();
				for (test = 0; test < numOfRequests; test++) {
					String url = "http://108.171.184.12/geoq/services/closest/?lon=-76.74545040246407&lat=18.00173855030801&limit=10";
					ProxyCache.doNotCache(url);

					updateTransportList();

					final long start = System.currentTimeMillis();
					GLNetworkManager.get(url, new GLNetworkAction() {
						public void success(Object data, int status) {
							incrementSuccesses();
							long end = System.currentTimeMillis();
							addResponseTime(end - start);
						}

						public void failure(Object data, int status) {
							incrementFailures();
							long end = System.currentTimeMillis();
						}
					});
					updateUi();
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						Utils.alert("Test stopped");
					}
				}
			}
		});
		networkTest.start();
	}

	private void addResponseTime(long ms) {
		totalResponseTime += ms;
	}

	private void updateUi() {
		Utils.invokeLater(new Runnable() {
			public void run() {
				_updateUi();
			}
		});
	}

	private void _initProgressBar() {
		progressBar.reset("", 0, getNumberOfRequests(), 0);
	}

	private void _updateUi() {
		successLabel.setText("Successes: " + successes);
		failuresLabel.setText("Failures: " + failures);
		progressBar.setValue(test);

		if (test > 0) {
			double avg = (double) totalResponseTime / test;
			responseTimeLabel.setText("Avg Resp Time: " + ((int) avg) + "ms");
		}

	}

	public void stopTests() {
		if (networkTest != null) {
			networkTest.interrupt();
		}
	}

	public void fieldChanged(Field field, int context) {
		if (field == startButton) {
			startTests();
		} else if (field == cancelButton) {
			stopTests();
		}
	}

	public void incrementSuccesses() {
		successes++;
	}

	public void incrementFailures() {
		failures++;
	}

}
