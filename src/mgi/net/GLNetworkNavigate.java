package mgi.net;

/**
 * This Class is setup to navigate a network by POSTING and GETTING
 * Data via a URL.
 * 
 * @author rahibbert
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import mgi.util.StoppableRunnable;
import mgi.util.Utils;
import net.rim.device.api.compress.GZIPInputStream;
import net.rim.device.api.io.http.HttpProtocolConstants;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.io.transport.ConnectionFactory;
import net.rim.device.api.io.transport.TransportInfo;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.CoverageStatusListener;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Memory;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.RadioButtonGroup;
import net.rim.device.api.util.Arrays;
import de.enough.glaze.ui.component.BasicEditField;
import de.enough.glaze.ui.component.RadioButtonField;
import de.enough.glaze.ui.container.VerticalFieldManager;

public class GLNetworkNavigate extends Thread implements GLNetwork,
		CoverageStatusListener {

	private static boolean useCompression = true;

	private static String apn = "";

	private Hashtable connectionDescriptors = new Hashtable();

	/**
	 * Stores requests that have been made. Connections object
	 * may or may not be opened for them.  
	 */
	private Hashtable requestsInProgress = new Hashtable();

	// List of all possible connections
	// protected ConnectionFactory factory = new ConnectionFactory();

	// Create preference ordered list of transports
	private int[] transportOrder = { TransportInfo.TRANSPORT_TCP_WIFI,
			TransportInfo.TRANSPORT_BIS_B,
			TransportInfo.TRANSPORT_TCP_CELLULAR, TransportInfo.TRANSPORT_WAP2,
			TransportInfo.TRANSPORT_MDS };

	// Holds all available connection methods
	private int[] transportsAvailable = {};

	private Hashtable requestListeners = new Hashtable();

	private Vector allRequestListeners = new Vector();

	/**
	 * Once we have a connection object for a url, it is put here.
	 */
	private Hashtable requestConnections = new Hashtable();


	private long checkTransportsEvery = 20000;

	private int nullDescriptors = 0;

	private int validDescriptors = 0;

	private static boolean useConnectionFactory = false;

	public GLNetworkNavigate() {
		// Remove any transports that are not (currently) available
		configureAvailableTransports();
		autoReconfigureTransports(); // check every
	}

	/**
	 * Runs a thread that auto checks and updates the transport order list
	 */

	private void autoReconfigureTransports() {
		Utils.startThread(new StoppableRunnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(checkTransportsEvery);
						configureAvailableTransports();
					} catch (InterruptedException e) {
					}
				}
			}
		});
	}

	/**
	 * Configures the available transports
	 */
	private void configureAvailableTransports() {
		Utils.log("Network: Configuring transports");
		String transportList = "";
		synchronized (transportsAvailable) {
			transportsAvailable = new int[0];
			for (int a = 0; a < transportOrder.length; a++) {
				int transport = transportOrder[a];
				if (DeviceInfo.isSimulator()
						|| (TransportInfo.isTransportTypeAvailable(transport) && TransportInfo
								.hasSufficientCoverage(transport))) {
					String msg = "";
					switch (transport) {
					case TransportInfo.TRANSPORT_TCP_WIFI:
						msg = "WIFI";
						transportList += " " + msg;
						break;

					case TransportInfo.TRANSPORT_BIS_B:
						msg = "BIS";
						transportList += " " + msg;
						break;

					case TransportInfo.TRANSPORT_WAP2:
						msg = "WAP2";
						transportList += " " + msg;
						break;

					case TransportInfo.TRANSPORT_TCP_CELLULAR:
						msg = "TCP";
						transportList += " " + msg;
						break;

					case TransportInfo.TRANSPORT_MDS:
						msg = "MDS";
						transportList += " " + msg;
						break;
					}
					Arrays.add(transportsAvailable, transport);
				}
			}
		}
		Utils.log("Network: Transports available with coverage: "
				+ transportList);
	}

	/**
	 * Builds and configures a connection factory for the client
	 * 
	 * @return
	 */
	public ConnectionFactory getConnectionFactory() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setPreferredTransportTypes(transportsAvailable);
		return factory;
	}

	/**
	 * Move a transport down by...
	 * 
	 * @param transportType
	 * @param down
	 */
	protected void demoteTransport(int transportType) {

		synchronized (transportOrder) {
			int numTransports = transportOrder.length;
			if (numTransports > 2) {
				// first, find the index of the transport type
				int index = -1;
				for (int i = 0; i < transportOrder.length; i++) {
					int otherTransportType = transportOrder[i];
					if (otherTransportType == transportType) {
						index = i;
						break;
					}
				}

				if (index != -1 && (index + 2) < numTransports) {
					int transportBelow = transportOrder[index + 1];
					int transportBelowThat = transportOrder[index + 2];
					preferTransport(transportBelowThat);
					preferTransport(transportBelow);
				}
			}
		}
	}

	/**
	 * Move a transport to the top of the list
	 * 
	 * @param transportType
	 */
	protected void preferTransport(int transportType) {
		synchronized (transportOrder) {
			int numTransports = transportOrder.length;
			if (numTransports > 0) {
				int firstTransport = transportOrder[0];
				if (firstTransport == transportType) {
					// nothing to do
					Utils.log("Network: No reordering necessary. "
							+ TransportInfo.getTransportTypeName(transportType)
							+ " is first");
					return;
				} else {
					int[] newTransports = new int[numTransports];
					int index = 1;
					Utils.log("Network: Reordering transports: #1 "
							+ TransportInfo.getTransportTypeName(transportType));
					newTransports[0] = transportType;
					for (int i = 0; i < numTransports; i++) {
						int oldTransportType = transportOrder[i];
						if (oldTransportType != transportType) {
							Utils.log("Network: Reordering transports: #"
									+ (index + 1)
									+ " "
									+ TransportInfo
											.getTransportTypeName(oldTransportType));
							newTransports[index] = oldTransportType;
							index++;
						}
					}
					// Replace the transport order array..
					this.transportOrder = newTransports;
				}
			}
		}
	}

	public String[] getAvailableConnectionTypes() {
		String[] types = new String[transportsAvailable.length];
		for (int i = 0; i < transportsAvailable.length; i++) {
			types[i] = TransportInfo
					.getTransportTypeName(transportsAvailable[i]);
		}
		return types;
	}

	/**
	 * Checks if any of the transports marked available are indeed available
	 */
	public boolean canConnect() {
		// Set ConnectionFactory options
		if (transportsAvailable.length > 0) {
			for (int i = 0; i < transportsAvailable.length; i++) {
				int transportType = transportsAvailable[i];
				if (TransportInfo.isTransportTypeAvailable(transportType)
						&& TransportInfo.hasSufficientCoverage(transportType)) {
					return true;
				}
			}
		}
		return false;
	}

	public void get(String url, final GLNetworkAction action) {
		get(url, null, action, false, false);
	}

	public void get(String url, Hashtable data, final GLNetworkAction action) {
		get(url, null, action, false, false);
	}

	public void post(String url, Hashtable data, final GLNetworkAction action) {
		get(url, data, action, true, false);
	}

	public void postMultiPart(String url, Hashtable data,
			final GLNetworkAction action) {
		get(url, data, action, true, true);
	}

	/**
	 * Gets or post data
	 * 
	 * @param url
	 *            - the url to post/get data to
	 * @param data
	 *            - the data to be sent
	 * @param action
	 *            - the action to be called back
	 * @param usePost
	 *            - if true, we'll use POST
	 * @param isMultiPart
	 *            - is multipart data
	 */
	protected void get(String url, Hashtable data,
			final GLNetworkAction action, boolean usePost, boolean isMultiPart) {
		// Save the original url
		final String originalUrl = url;
		
		// Exit based on certain conditions ===================================
		if (url == null) {
			Utils.err("Network: Asked to get NULL url, exiting");
			if (notifyListenersThatRequestStopped(originalUrl, action)) {
				return;
			}
		}

		if (!canConnect()) {
			Utils.log("Network: Cannot connect to the net currently");
			action.urlUnreachable();
			if (notifyListenersThatRequestStopped(originalUrl, action)) {
				return;
			}
		}

		// Initial config =====================================================
		if (!usePost) { // don't attach the data to the url if we're using post
			url = Utils.getUrl(url, data);
		}
		
		if(action != null){
			action.setUrl(originalUrl); // set the url
			action.setRequest(this);
		}

		final String currentUrl = url + (url.endsWith("/") ? "?" : "");
		
		
		// Request started ====================================================
		requestedStarted(originalUrl);

		if (ProxyCache.isCached(url) && !usePost) { // If we had previously
			// cached this...
			Utils.log("Network: Returning response from cache");
			String response = ProxyCache.getResponse(url);
			byte[] rawData = ProxyCache.getBytes(url);
			configureAction(action, originalUrl, response, rawData, 200);
			requestedCompleted(originalUrl);
			return;
		}

		long start = System.currentTimeMillis();
		long factoryConnectionTime = 0;
		String transportName = "Unknown";
		ConnectionDescriptor connectionDescriptor = null;
		int transportType = transportOrder[0];
		if (useConnectionFactory) { // Connection factory =====================
			if (notifyListenersThatRequestStopped(originalUrl,action)) {
				return;
			}
			connectionDescriptor = getConnectionDescriptor(url);
			incrementDescriptor(connectionDescriptor);
			factoryConnectionTime = System.currentTimeMillis() - start;
			Utils.log("Network: Opening connection took "
					+ factoryConnectionTime + "ms");
			if(action != null){
				action.setConnectionDescriptor(connectionDescriptor);
			}
			

			// Setup the action and connection descriptor
			Utils.log("Network: GET " + url);
			if (connectionDescriptor == null) {
				action.failure(null, -1);
				Utils.err("Network: Empty connection descriptor =============================================");
				Utils.log("Network: Demoting transport: "
						+ TransportInfo.getTransportTypeName(transportOrder[0]));
				demoteTransport(transportOrder[0]);
				notifyListenersThatRequestStopped(originalUrl,action);
				return;
			}

			transportType = connectionDescriptor.getTransportDescriptor()
					.getTransportType();

			transportName = TransportInfo.getTransportTypeName(transportType);

		}
		Utils.log("Network: Using " + transportName + " for this request");

		// Declare some variables
		HttpConnection httpConnection = null;
		InputStream reader = null;

		if (notifyListenersThatRequestStopped(originalUrl,action)) {
			return;
		}

		try {
			long startTime = System.currentTimeMillis();
			long getConnectionTime, getResponseTime, getDataTime, unzipTime, responseTime;
			if (useConnectionFactory) {
				httpConnection = (HttpConnection) connectionDescriptor
						.getConnection();
			} else {
				String connectionString = getConnectionString();
				String targetUrl = currentUrl + connectionString;
				Utils.log("[Network] Requesting url via conn string method: " + targetUrl);
				httpConnection = (HttpConnection) Connector.open(targetUrl);
			}

			if (httpConnection == null) {
				Utils.err("[Network] NULL Http Connection received for " + originalUrl + ", returning...");
				stopRequest(currentUrl);
				return;
			}
			
			getConnectionTime = System.currentTimeMillis();
			connectionOpened(originalUrl, httpConnection); // Addd the connection object

			if (usePost) { // set POST if we're using it
				httpConnection
						.setRequestMethod(HttpProtocolConstants.HTTP_METHOD_POST);
			}

			// Configure the headers
			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_USER_AGENT,
					"Blackberry Device");
			httpConnection
					.setRequestProperty("x-rim-transcode-content", "none");

			// Set headers if GZIP is enabled...
			if (useCompression) {
				httpConnection
						.setRequestProperty(
								HttpProtocolConstants.HEADER_CONTENT_TYPE,
								HttpProtocolConstants.CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED);
				httpConnection.setRequestProperty(
						HttpProtocolConstants.HEADER_ACCEPT_ENCODING, "gzip");
			}

			if (usePost && data != null) { // write the data out
				httpConnection.setRequestProperty(
						HttpProtocolConstants.HEADER_CONTENT_ENCODING, "gzip");

				String boundary = "-----BBPayloadBoundary12345876jz8";
				httpConnection.setRequestProperty(
						HttpProtocolConstants.HEADER_CONTENT_TYPE,
						HttpProtocolConstants.CONTENT_TYPE_MULTIPART_FORM_DATA
								+ " ;boundary=" + boundary);

				ByteArrayOutputStream bos = new ByteArrayOutputStream();

				String newLine = "\r\n";

				if (notifyListenersThatRequestStopped(originalUrl, action)) {
					return;
				}

				
				if (!isMultiPart) {
					Enumeration keys = data.keys();
					while (keys.hasMoreElements()) {
						String key = keys.nextElement().toString();
						String value = data.get(key).toString();
						bos.write(newLine.getBytes());
						bos.write("--".getBytes());
						bos.write(boundary.getBytes());
						bos.write(newLine.getBytes());
						bos.write(("Content-Disposition:form-data; name=\""
								+ key + "\"").getBytes());
						bos.write(newLine.getBytes()); // then two new
														// lines
						bos.write(newLine.getBytes());
						bos.write(value.getBytes());
					}
				} else { // we're posting multipart data
					Enumeration keys = data.keys();

					// Get a total of all the content we need to send
					Enumeration elements = data.elements();
					int total = 0;
					int progress = 0;

					// Calculate the total number of bytes we'll be sending
					while (elements.hasMoreElements()) {
						Object obj = elements.nextElement();
						if (obj.getClass().isArray()) {
							total += ((byte[]) obj).length;
						}
					}

					while (keys.hasMoreElements()) { // send up the binary values

						if (notifyListenersThatRequestStopped(originalUrl, action)) {
							return;
						}

						
						String key = keys.nextElement().toString();
						Object obj = data.get(key);
						byte[] value;
						boolean isByteArray = obj.getClass().isArray();
						if (isByteArray) {
							value = (byte[]) data.get(key);
						} else {
							value = obj.toString().getBytes();
						}

						try {
							bos.write(newLine.getBytes()); // start with \n
							bos.write("--".getBytes());
							bos.write(boundary.getBytes()); // put the
															// boundary
							bos.write(newLine.getBytes()); // another \n
							bos.write(("Content-Disposition:form-data; name=\""
									+ key + "\";" + " filename=\"user_file_"
									+ System.currentTimeMillis() + ".jpg\"")
									.getBytes());
							bos.write(newLine.getBytes()); // another \n
							bos.write(("Content-Type:application/octet-stream")
									.getBytes());
							bos.write(newLine.getBytes()); // another \n
							bos.write("Content-Transfer-Encoding: binary"
									.getBytes());
							bos.write(newLine.getBytes()); // then two new
															// lines
							bos.write(newLine.getBytes());
							// Write the binary file....
							bos.write(value);
							
						} catch(OutOfMemoryError oome){
							Utils.err("[Network] OUT OF MEMORY ERROR ===================================");
							Utils.err("[Network] Could not complete upload to: " + currentUrl);
							Utils.err("[Network] Last value: " + value);
							notifyListenersThatRequestStopped(originalUrl, action);
							oome.printStackTrace();
							bos = null;
							value = null;
							return;
						} catch (Exception e) {
							Utils.err("[Network] EXCEPTION ===================================");
							Utils.err("[Network] Could not complete upload to: " + currentUrl);
							Utils.err("[Network] Last value: " + value);
							notifyListenersThatRequestStopped(originalUrl, action);
							bos = null;
							value = null;
							e.printStackTrace();
							return;
						}
					}
				}

				if (notifyListenersThatRequestStopped(originalUrl, action)) {
					return;
				}

				// terminate header
				bos.write(newLine.getBytes());
				bos.write("--".getBytes());
				bos.write(boundary.getBytes()); // put the boundary
				bos.write("--".getBytes());
				bos.write(newLine.getBytes());

				// Store all the POST/GET data in an array...
				byte[] finalData = bos.toByteArray();
				bos.close();
				int len = finalData.length;
				int dataWritten = 0;
				httpConnection.setRequestProperty(
						HttpProtocolConstants.HEADER_CONTENT_LENGTH,
						Integer.toString(len));
				
				Utils.log("[Network] Opening outputstream to: " + originalUrl);
				OutputStream os = httpConnection.openOutputStream();

				if (notifyListenersThatRequestStopped(originalUrl, action)) {
					return;
				}

				while (dataWritten < len) {
					
					if (notifyListenersThatRequestStopped(originalUrl, action)) {
						return;
					}

					
					int dataToWrite = Math.min(256, len - dataWritten);
					os.write(finalData, dataWritten, dataToWrite);
					dataWritten += dataToWrite;

					final int currentProgress = dataWritten;
					final int currentTotal = len;

//					if (dataWritten % (256 * 4 * 50) == 0) { // notify
//																// people
//																// every
//																// 50KB
//																// written
//						requestedProgressed(originalUrl, currentProgress,
//								currentTotal);
//					}
				}
				
				Utils.log("[Network] Closing outputstream to: " + originalUrl);
				
				// os.write(finalData, 0, len);
				long startCloseStream = System.currentTimeMillis();
				os.flush();
				os.close();
				long endCloseStream = System.currentTimeMillis();
				Utils.log("[Network] Took " + (endCloseStream-startCloseStream)/1000 + "s to close output stream to " + url);
				bos = null;
				os = null;
				finalData = null;
				dataWritten = 0;
			}

			// Get the response code
			Utils.log("[Network] Fetching response code for " + originalUrl);
			int status = httpConnection.getResponseCode();
			Utils.log("[Network] Status Code: " + status);
			

			String contentEncoding = httpConnection.getEncoding();
			getResponseTime = System.currentTimeMillis();

			if (status == HttpConnection.HTTP_UNAUTHORIZED) {
				Utils.err("[Network] Cannot touch the Internet =======================================");
			} else {
				reader = httpConnection.openInputStream();
				String response;
				byte[] rawData;
				getDataTime = System.currentTimeMillis();

				// Now check on the encoding...
				if (contentEncoding != null && contentEncoding.equals("gzip")) {
					// The data is compressed
					GZIPInputStream gzipInputStream = new GZIPInputStream(
							reader);
					rawData = net.rim.device.api.io.IOUtilities
							.streamToBytes(gzipInputStream);
					unzipTime = System.currentTimeMillis();
				} else {
					unzipTime = getDataTime;
					rawData = net.rim.device.api.io.IOUtilities
							.streamToBytes(reader);
				}

				int size = rawData.length;

				if (size < 1000 * 1024) {
					response = new String(rawData);
					if(status == 500){
						Utils.err("[Network] Response from 500\n" + response);
					}
				} else {
					response = ""; // response truncated
				}

				responseTime = System.currentTimeMillis();
				// Now we have string and bytes saved
				unzipTime = unzipTime - getDataTime;
				getDataTime = getDataTime - getResponseTime;
				getResponseTime = getResponseTime - getConnectionTime;
				getConnectionTime = getConnectionTime - startTime;
				String mode = isUseConnectionFactory() ? "(using factory)" : "(using conn string)";
				Utils.log("[Network] Stats for: " + originalUrl + "\n  [" + mode + " " + transportName + ", " + size + "B] \n"
						+ "  Overall: " + (responseTime - startTime) + "\n"
						+ "  Connection: " + getConnectionTime + "\n"
						+ "  Response: " + getResponseTime + "\n" + "  Data: "
						+ getDataTime + "\n" + "  Unzip: " + unzipTime
						+ "\n  Factory Connection: " + factoryConnectionTime);

				// update the action
				configureAction(action, originalUrl, response, rawData, status);

				// clean up
				// is.close();
				httpConnection.close();
				// is = null;
				connectionDescriptor = null;

				// Cache the result... IF it's successful
				if (status == 200 && !isMultiPart) { // don't cache file uploads
					ProxyCache.cache(url, rawData, response);
				}

				

				// Prefer the transport given certain metrics...
				if (status == 200 && getResponseTime < 5000
						&& useConnectionFactory) {
					// prefer this transport...
					preferTransport(transportType);
				}
				
				if(status == 200){
					// Notify listeners that the request is complete
					Utils.startThread(new StoppableRunnable() {
						public void run() {
							requestedCompleted(originalUrl);
						}
					});	
				} else {
					// Notify listeners that the request failed
					Utils.startThread(new StoppableRunnable() {
						public void run() {
							requestedFailed(originalUrl);
						}
					});
				}

			}
		}
		catch (IOException ioe){
			action.urlUnreachable();
			requestedFailed(originalUrl);
			Utils.err("[Network] IOException occurred during  " + (usePost? "POST" : "GET") + "\n" 
					+ ioe.getMessage()
					+ " ==========================================");
			ioe.printStackTrace();
		}
		catch (Exception e) {
			action.urlUnreachable();
			requestedFailed(originalUrl);
			Utils.err("[Network] Exception occurred during " + (usePost? "POST" : "GET") + "\n"
					+ e.getMessage()
					+ " ==========================================");
			e.printStackTrace();
		} finally {
			// clean up
			if (reader != null) {
				try {
					reader.close();
					reader = null;
				} catch (IOException e) {
					String msg = e.getMessage();
					Utils.warn(msg);
				}
			}
		}
	}

	/**
	 * Notify listeners in a thread that the request has failed
	 * 
	 * @param originalUrl
	 * @return boolean - returns true if the request is inactive
	 */
	private boolean notifyListenersThatRequestStopped(final String url,
			GLNetworkAction action) {
		if (isRequestInActive(url)) {
			Utils.err("[Network] STOPPING request: " + url + "\n==========================================================");
			requestedFailed(url);

			if (action != null) {
				action.failure(null, 500);
			}

			return true;
		}
		return false;
	}

	/**
	 * Configures a connection factory and returns a connection descriptor
	 * object
	 * 
	 * @param url
	 * @return
	 */
	private ConnectionDescriptor getConnectionDescriptor(String url) {
		if (connectionDescriptors.get(url) != null) {
			ConnectionDescriptor descriptor = (ConnectionDescriptor) connectionDescriptors
					.get(url);
			// remove it...
			connectionDescriptors.remove(url);
			// check if descriptor is still available
			int transportType = descriptor.getTransportDescriptor()
					.getTransportType();
			if (TransportInfo.hasSufficientCoverage(transportType)) {
				return descriptor;
			}
		}
		ConnectionFactory factory = getConnectionFactory();
		return factory.getConnection(url);
	}

	/**
	 * Makes a prerequest and caches the descriptor
	 * 
	 * @param url
	 */
	public void preRequest(final String url) {
		Utils.log("Network: Will cache descriptor for: " + url);
		Utils.startThread(new StoppableRunnable() {
			public void run() {

				if (isUseConnectionFactory()) {
					ConnectionFactory factory = getConnectionFactory();
					long start = System.currentTimeMillis();
					ConnectionDescriptor descriptor = factory
							.getConnection(url);
					long end = System.currentTimeMillis();
					if (descriptor != null) {
						connectionDescriptors.put(url, descriptor);
						Utils.log("Network: Cached descriptor for " + url
								+ " in " + (end - start) + "ms");
					} else {
						Utils.warn("Network: Discarded NULL descriptor for "
								+ url + " in " + (end - start) + "ms");
					}
				} else {
					// Just request it...
					final long start = System.currentTimeMillis();
					get(url, new GLNetworkAction() {
						public void success(Object data, int status) {
							long end = System.currentTimeMillis();
							Utils.log("Network: Cached " + url + " in "
									+ (end - start) + "ms");
						};
					});
				}
			}
		});
	}

	protected void configureAction(GLNetworkAction action, String url,
			String response, byte[] rawData, int status) {
		action.setRawResponse(rawData);
		action.setResponse(response);
		action.setStatus(status);
		action.setResposnseSuccess(status == HttpConnection.HTTP_OK);

		// call the callbacks
		if (status != HttpConnection.HTTP_OK) {
			GLNetworkManager.incrementNetworkFailure();
			Utils.warn("Network: Request failed: " + url + " [code=" + status
					+ "] ==========================================");
			action.failure(response, status);
		} else {
			action.success(response, status);
		}
	}

	/**
	 * @deprecated
	 */

	public void coverageStatusChanged(int newCoverage) {
		// TODO Auto-generated method stub

	}

	public void addRequestListener(GLNetworkRequestListener listener) {
		allRequestListeners.addElement(listener);
	}
	
	/**
	 * Removes a network request listener
	 * @param listener
	 * @return
	 */
	public boolean removeRequestListener(GLNetworkRequestListener listener){
		return allRequestListeners.removeElement(listener);
	}

	/**
	 * Adds a request listener for a url
	 * 
	 * @param url
	 * @param listener
	 */
	public void addRequestListener(String url, GLNetworkRequestListener listener) {
		Vector listeners = Utils.getValue(requestListeners, url, new Vector());
		listeners.addElement(listener);
		requestListeners.put(url, listeners);
	}

	/**
	 * Returns a list of request listeners
	 * 
	 * @param url
	 * @return
	 */
	public Vector getRequestListeners(String url) {
		Vector listeners = Utils.getValue(requestListeners, url, new Vector());

		for (int i = 0; i < allRequestListeners.size(); i++) {
			GLNetworkRequestListener listener = (GLNetworkRequestListener) allRequestListeners
					.elementAt(i);
			// make sure we don't duplicate em
			boolean listenerExists = false;
			for (int j = 0; j < listeners.size(); j++) {
				GLNetworkRequestListener urlListener = (GLNetworkRequestListener) listeners
						.elementAt(j);
				if (urlListener == listener) {
					listenerExists = true;
				}
			}

			if (!listenerExists) {
				listeners.addElement(listener);
			}
		}
		return listeners;
	}

	/**
	 * Notifies that listeners that a request has started
	 * 
	 * @param url
	 */
	protected void requestedStarted(final String url) {
		synchronized (requestsInProgress) {
			Utils.log("[URL] Url now in progress: " + url);
			requestsInProgress.put(url, url);
		}
		
		Vector listeners = getRequestListeners(url);
		for (int i = 0; i < listeners.size(); i++) {
			GLNetworkRequestListener listener = (GLNetworkRequestListener) listeners
					.elementAt(i);
			listener.requestStarted(url);
		}
	}
	
	
	
	
	/**
	 * Notifies that listeners that a request has failed
	 * 
	 * @param url
	 */
	protected void requestedFailed(final String url) {
		Vector listeners = getRequestListeners(url);
		for (int i = 0; i < listeners.size(); i++) {
			GLNetworkRequestListener listener = (GLNetworkRequestListener) listeners
					.elementAt(i);
			listener.requestFailed(url);
		}
		synchronized (requestsInProgress) {
			requestsInProgress.remove(url);
		}
		
		synchronized (requestConnections) {
			requestConnections.remove(url);
		}
	}

	/**
	 * Notifies listeners that a request is completed
	 * 
	 * @param url
	 */
	protected void requestedCompleted(final String url) {
		Vector listeners = getRequestListeners(url);
		for (int i = 0; i < listeners.size(); i++) {
			GLNetworkRequestListener listener = (GLNetworkRequestListener) listeners
					.elementAt(i);
			listener.requestComplete(url);
		}
		synchronized (requestsInProgress) {
			requestsInProgress.remove(url);
		}
		
		synchronized (requestConnections) {
			requestConnections.remove(url);
		}
	}

	/**
	 * Notify listeners that a request has progressed
	 * 
	 * @param url
	 * @param progress
	 * @param total
	 */
	protected void requestedProgressed(final String url, final int progress,
			final int total) {
		Vector listeners = getRequestListeners(url);
		for (int i = 0; i < listeners.size(); i++) {
			GLNetworkRequestListener listener = (GLNetworkRequestListener) listeners
					.elementAt(i);
			listener.requestProgressed(url, progress, total);
		}
		
		Utils.log("[Memory] Free memory: " + Memory.getRAMStats().getFree()/1000000 + "MB");
	}

	/**
	 * Returns true if a url has been requested but we have not 
	 * opened a connection for it yet. 
	 * @param url
	 * @return
	 */
	public boolean isRequestInProgress(String url) {
		synchronized (requestsInProgress) {
			return requestsInProgress.get(url) != null;
		}
	}

	
	/**
	 * Adds the connection object for the url 
	 * @param url
	 * @param connection
	 */
	protected void connectionOpened(String url, HttpConnection connection) {
		requestConnections.put(url, connection);
	}

	protected void removeRequestConnection(String url) {
		requestConnections.remove(url);
	}

	/**
	 * Returns true if we have opened a connection for a url already.
	 * @param url
	 * @return
	 */
	public boolean isConnectionInProgress(String url) {
		return requestConnections.get(url) != null;
	}

	public void stopRequest(String url) {
		Utils.log("Network: Stopping url request: " + url);
		Utils.log("[URL] Status: " + url + "\n\tConnection: " + (isConnectionInProgress(url) ? "YES" : "NO" ) + "\n\tRequest: " + (isRequestInProgress(url) ? "YES" : "NO") + "\n");
		
		if(isRequestInProgress(url)){
			requestsInProgress.remove(url);
		}
		
		if (isConnectionInProgress(url)) {
			HttpConnection httpConnection = (HttpConnection) requestConnections
					.get(url);
			
			try {
				httpConnection.close();
				Utils.log("Network: Stopped url request: " + url);
			} catch (IOException e) {
				Utils.log("Network: IOException while stopping url request: "
						+ url);
				e.printStackTrace();
			}
			requestConnections.remove(url);
		}
		
		requestedFailed(url);
	}
	
	/**
	 * Returns true if the request is not active
	 * @return
	 */
	public boolean isRequestInActive(String url){
		return !isConnectionInProgress(url) && !isRequestInProgress(url);
	}

	/**
	 * Keeps record of the number of descriptors we have that are valid versus
	 * the null ones
	 * 
	 * @param descriptor
	 */
	private void incrementDescriptor(ConnectionDescriptor descriptor) {
		if (descriptor == null) {
			nullDescriptors++;
			Utils.warn("Network: We now have gotten " + nullDescriptors
					+ " null descriptors. (" + validDescriptors
					+ " were valid)");
		} else {
			validDescriptors++;
		}
	}

	public String getDescriptorReport() {
		String str = validDescriptors + " valid descriptor(s)\n"
				+ nullDescriptors + " null descriptor(s)";
		return str;
	}

	public static String getConnectionString() {

		String connectionString = null;

		// Simulator behavior is controlled by the USE_MDS_IN_SIMULATOR
		// variable.
		if (DeviceInfo.isSimulator()) {
			// Don't use MDS
			connectionString = ";deviceside=true";
		}

		// Wifi is the preferred transmission method
		else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
			Utils.log("[Network] Device is connected via Wifi.");
			connectionString = ";interface=wifi";
		}

		// Is the carrier network the only way to connect?
		else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {

			String carrierUid = getCarrierBIBSUid();
			if (carrierUid == null) {
				// Has carrier coverage, but not BIBS. So use the carrier's TCP
				// network
				Utils.log("[Network] No UID found");
				connectionString = ";deviceside=true";
			} else {
				// otherwise, use the Uid to construct a valid carrier BIBS
				// request
				Utils.log("[Network] uid is: " + carrierUid);
				connectionString = ";deviceside=false;connectionUID="
						+ carrierUid + ";";
			}
		}

		// Check for an MDS connection instead (BlackBerry Enterprise Server)
		else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {
			Utils.log("[Network] MDS coverage found");
			connectionString = ";deviceside=false";
		}

		// If there is no connection available abort to avoid bugging the user
		// unnecssarily.
		else if (CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE) {
			Utils.err("[Network] There is no available connection.");
		}

		// In theory, all bases are covered so this shouldn't be reachable.
		else {
			Utils.log("[Network] No other options found, assuming device.");
			connectionString = ";deviceside=true";
		}

		return connectionString;
	}

	/**
	 * Looks through the phone's service book for a carrier provided BIBS
	 * network
	 * 
	 * @return The uid used to connect to that network.
	 */
	private static String getCarrierBIBSUid() {
		ServiceRecord[] records = ServiceBook.getSB().getRecords();
		int currentRecord;

		for (currentRecord = 0; currentRecord < records.length; currentRecord++) {
			if (records[currentRecord].getCid().toLowerCase().equals("ippp")) {
				if (records[currentRecord].getName().toLowerCase()
						.indexOf("bibs") >= 0) {
					return records[currentRecord].getUid();
				}
			}
		}

		return null;
	}

	public static String getApn() {
		return apn;
	}

	public static void setApn(String apn) {
		if (apn == null) {
			apn = "";
		}
		GLNetworkNavigate.apn = apn;
	}

	public static boolean isUseConnectionFactory() {
		return useConnectionFactory;
	}

	public static void setUseConnectionFactory(boolean useConnectionFactory) {
		GLNetworkNavigate.useConnectionFactory = useConnectionFactory;
	}

	public static void addNetworkSettings(VerticalFieldManager settingsVfm) {
		// Add the connection factory or connection string option...
		RadioButtonGroup connectionGrp = new RadioButtonGroup();
		final RadioButtonField useFactory = new RadioButtonField(
				"Use Connection Factory", connectionGrp,
				isUseConnectionFactory());
		final RadioButtonField useConnStrings = new RadioButtonField(
				"Use Connection Strings", connectionGrp,
				!isUseConnectionFactory());

		useFactory.setChangeListener(new FieldChangeListener() {

			public void fieldChanged(Field field, int context) {
				GLNetworkNavigate.setUseConnectionFactory(useFactory
						.isSelected());
			}
		});

		useConnStrings.setChangeListener(new FieldChangeListener() {

			public void fieldChanged(Field field, int context) {
				GLNetworkNavigate.setUseConnectionFactory(useConnStrings
						.isSelected());
			}
		});

		settingsVfm.add(useFactory);
		settingsVfm.add(useConnStrings);

		// Add some settings for the APN
		final BasicEditField apnField = new BasicEditField();
		apnField.setLabel("APN");

		apnField.setChangeListener(new FieldChangeListener() {

			public void fieldChanged(Field field, int context) {
				String apn = apnField.getText();
				Utils.log("[Network] Updating apn to: " + apn);
				setApn(apn);
			}
		});

		settingsVfm.add(apnField);

	}
	


}
