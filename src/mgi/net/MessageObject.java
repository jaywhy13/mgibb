package mgi.net;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import mgi.events.GLEvents;
import mgi.json.JSONArray;
import mgi.json.JSONException;
import mgi.json.JSONObject;
import mgi.util.Utils;

/**
 * This class encapsulates a message that the server speaks
 * 
 * @author jay
 * 
 */
public class MessageObject {

	public static final int MESSAGE_NOTIFICATION = 1;
	public static final int MESSAGE_WARNING = 2;
	public static final int MESSAGE_ERROR = 3;
	public static final int MESSAGE_CUSTOM = 4;
	public static final int MESSAGE_DEBUG = 5;
	public static final int MESSAGE_ARRAY = 6;
	public static final int MESSAGE_MULTIPLE = 7;

	private int messageType = MESSAGE_NOTIFICATION;

	private JSONObject json = new JSONObject();
	
	public JSONObject getJson() {
		return json;
	}

	private void setJson(JSONObject json) {
		this.json = json;
	}

	/**
	 * Stores multiple message objects
	 */
	private Vector messageObjects = new Vector();

	private String message = "";

	private String[] array = new String[] {};

	private Hashtable params = new Hashtable();

	public MessageObject(int messageType) {
		this.messageType = messageType;
	}

	public MessageObject(int messageType, String message) {
		this.messageType = messageType;
		this.message = message;
	}
	
	public static MessageObject fromUrl(String url){
		return fromUrl(url, null, null, null);
	}
	
	public static MessageObject fromPostMultiPart(final String url, Hashtable data){
		final MessageObject mo = new MessageObject(MESSAGE_MULTIPLE);
		
		GLNetwork network = GLNetworkManager.getNetworkManager();
		
		network.postMultiPart(url, data, new GLNetworkAction() {
			public void success(Object data, int status) {
				MessageObject responseMo = fromJson(data.toString());
				mo.addMessageObject(responseMo);
			}

			public void failure(Object data, int status) {
			}
		});


		// check if we have a message object at index 0
		if (mo.getMessageObject(0) != null) {
			return mo.getMessageObject(0);
		} else {
			return new MessageObject(MESSAGE_ERROR, "Unknown error occurred");
		}
	}
	
	
	public static MessageObject fromPost(final String url, Hashtable data){
		final MessageObject mo = new MessageObject(MESSAGE_MULTIPLE);
		
		GLNetwork network = GLNetworkManager.getNetworkManager();
		
		network.post(url, data, new GLNetworkAction() {
			public void success(Object data, int status) {
				MessageObject responseMo = fromJson(data.toString());
				mo.addMessageObject(responseMo);
			}

			public void failure(Object data, int status) {
			}
		});


		// check if we have a message object at index 0
		if (mo.getMessageObject(0) != null) {
			return mo.getMessageObject(0);
		} else {
			return new MessageObject(MESSAGE_ERROR, "Unknown error occurred");
		}
	}

	/**
	 * Grabs a message object from a url, returns null if anything goes wrong
	 * 
	 * @param url
	 * @return
	 */
	public static MessageObject fromUrl(final String url, final String startEvent, final String successEvent, final String failureEvent) {
		final MessageObject mo = new MessageObject(MESSAGE_MULTIPLE);

		GLEvents.triggerEvent(startEvent);
		final long startTime = System.currentTimeMillis();
		
		boolean result = GLNetworkManager.get(url, new GLNetworkAction() {
			public void success(Object data, int status) {
				long endRequestTime = System.currentTimeMillis();
				MessageObject responseMo = fromJson(data.toString());
				long constructTime = System.currentTimeMillis();
				Utils.debug("Message object parsing for: " + url);
				Utils.debug("Request took " + (endRequestTime-startTime) + "ms, parsing took " + (constructTime-endRequestTime) + "ms"); 
				mo.addMessageObject(responseMo);
			}

			public void failure(Object data, int status) {
			}
		});

		if (!result) {
			Utils.err("Cannot connect to retrieve json for message object");
		}

		// check if we have a message object at index 0
		if (mo.getMessageObject(0) != null) {
			GLEvents.triggerEvent(successEvent);
			return mo.getMessageObject(0);
		} else {
			GLEvents.triggerEvent(failureEvent);
			return new MessageObject(MESSAGE_ERROR, "Unknown error occurred");
		}
	}

	public static MessageObject fromJson(JSONObject json) {
		int messageType = Utils.getValue(json, "type", MESSAGE_NOTIFICATION);
		MessageObject mo = new MessageObject(messageType);
		mo.setJson(json);

		// Add params for everything else we added...
		Enumeration keys = json.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			String value = Utils.getValue(json, key, "");
			mo.addParam(key, value);
		}

		switch (messageType) {
		case MESSAGE_NOTIFICATION:
		case MESSAGE_WARNING:
		case MESSAGE_ERROR:
		case MESSAGE_DEBUG:
			mo.setMessage(Utils.getValue(json, "message", ""));
			break;

		case MESSAGE_CUSTOM:
			break;
		case MESSAGE_ARRAY:
			JSONArray jsonArr = Utils.getValue(json, "array", new JSONArray());
			String[] arr = new String[jsonArr.length()];
			for (int i = 0; i < jsonArr.length(); i++) {
				try {
					arr[i] = jsonArr.getString(i);
				} catch (JSONException e) {
					arr[i] = "";
				}
			}
			mo.setArray(arr);
			break;
		case MESSAGE_MULTIPLE:
			JSONArray moArr = Utils.getValue(json, "messages", new JSONArray());
			for (int i = 0; i < moArr.length(); i++) {
				JSONObject obj = Utils.getValue(moArr, i, null);
				if (obj != null) {
					MessageObject arrMo = fromJson(obj);
					mo.addMessageObject(arrMo);
				}
			}
		}
		return mo;
	}

	public static MessageObject fromJson(String jsonStr) {
		try {
			MessageObject mo = fromJson(new JSONObject(jsonStr));
			return mo;
		} catch (JSONException e) {
			Utils.warn("Invalid json received: " + jsonStr);
			return null;
		}
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public Vector getMessageObjects() {
		return messageObjects;
	}

	public void setMessageObjects(Vector messageObjects) {
		this.messageObjects = messageObjects;
	}

	public void addMessageObject(MessageObject mo) {
		this.messageObjects.addElement(mo);
	}

	public MessageObject getMessageObject(int index) {
		if (index < messageObjects.size()) {
			return (MessageObject) messageObjects.elementAt(index);
		}
		return null;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void addParam(String key, String value) {
		if (value != null) {
			params.put(key, value);
		}

	}

	public boolean hasParam(String key) {
		return params.containsKey(key);
	}

	public String getParam(String key) {
		if (hasParam(key)) {
			return params.get(key).toString();
		}
		return null;
	}
	
	public String getParam(String key, String def){
		if (hasParam(key)) {
			return params.get(key).toString();
		}
		return def;
	}

	public boolean paramEquals(String key, String value) {
		if (hasParam(key)) {
			return getParam(key).equals(value);
		}
		return false;
	}

	public String[] getArray() {
		return array;
	}

	public void setArray(String[] array) {
		this.array = array;
	}

	public boolean is(int messageType) {
		return this.messageType == messageType;
	}

	public boolean has(int messageType) {
		return contains(messageType);
	}

	public boolean contains(int messageType) {
		if (this.messageType == MESSAGE_MULTIPLE) {
			Vector messageObjects = getMessageObjects();
			for (int i = 0; i < messageObjects.size(); i++) {
				MessageObject mo = (MessageObject) messageObjects.elementAt(i);
				if (mo.is(messageType)) {
					return true;
				}
			}
		} else {
			return is(messageType);
		}
		return false;
	}

	public boolean isNotification() {
		return is(MESSAGE_NOTIFICATION);
	}

	public boolean isError() {
		return is(MESSAGE_ERROR);
	}

	public boolean isWarning() {
		return is(MESSAGE_WARNING);
	}

	public boolean isCustom() {
		return is(MESSAGE_CUSTOM);
	}

	public boolean isDebug() {
		return is(MESSAGE_DEBUG);
	}

	public boolean isArray() {
		return is(MESSAGE_ARRAY);
	}

	public boolean isMultiple() {
		return is(MESSAGE_MULTIPLE);
	}
	
	public String toString() {
		try {
			return getJson().toString(3);
		} catch (JSONException e) {
			return "[MessageObject] Could not print out json";
		}
	}

}
