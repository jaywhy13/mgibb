package mgi.net;

import java.util.Hashtable;

public class ProxyCache {

	private static Hashtable byteCache = new Hashtable();
	private static Hashtable responseCache = new Hashtable();
	private static Hashtable blackList = new Hashtable();

	/**
	 * Caches the result of a request
	 * @param url
	 * @param rawData
	 * @param response
	 */
	public static void cache(String url, byte[] rawData, String response) {
		if (!isExcludedFromCache(url)) {
			byteCache.put(url, rawData);
			responseCache.put(url, response);
		}
	}

	/**
	 * Returns true if the request is cached
	 * @param url
	 * @return
	 */
	public static boolean isCached(String url) {
		return byteCache.get(url) != null && responseCache.get(url) != null;
	}

	/**
	 * Gets the string response for the request
	 * @param url
	 * @return
	 */
	public static String getResponse(String url) {
		if (isCached(url)) {
			return responseCache.get(url).toString();
		}
		return null;
	}

	/**
	 * Gets the raw bytes from each request
	 * @param url
	 * @return
	 */
	public static byte[] getBytes(String url) {
		if (isCached(url)) {
			return (byte[]) byteCache.get(url);
		}
		return null;
	}

	/**
	 * Excludes a url from the cache
	 * @param url
	 */
	public static void excludeFromCache(String url) {
		blackList.put(url, url);
	}
	
	public static void doNotCache(String url){
		excludeFromCache(url);
	}

	/**
	 * Checks if a url is excluded from the cache
	 * @param url
	 * @return
	 */
	public static boolean isExcludedFromCache(String url) {
		return blackList.containsKey(url);
	}
	
	/**
	 * Removes a url from the cache
	 * @param url
	 */
	public static void removeFromCache(String url){
		byteCache.remove(url);
		responseCache.remove(url);
	}

}
