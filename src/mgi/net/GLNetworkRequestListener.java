package mgi.net;

public interface GLNetworkRequestListener {
	
	public void requestProgressed(String url, int progress, int total);
	
	public void requestComplete(String url);
	
	public void requestStarted(String url);
	
	public void requestFailed(String url);

}
