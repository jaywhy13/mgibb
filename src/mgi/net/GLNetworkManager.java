package mgi.net;

import java.util.Hashtable;

import mgi.util.Utils;

public class GLNetworkManager {
	
	private static GLNetwork nwork;
	
	private static int maximumNetworkFailures = 2;
	
	private static int urlTimeout = 4000;
	
	private static int networkFailures = 0;
	
	private static boolean enabled = true;
	
	public static boolean canConnect(){
		GLNetwork network = getNetworkManager();
		return network.canConnect();
	}
	
	public static boolean get(String url, GLNetworkAction action){
		return get(url, null, action);
	}
	
	public static boolean get(String url, Hashtable data, GLNetworkAction action){
		if(getNetworkManager() != null){
			if(getNetworkManager().canConnect()){
				getNetworkManager().get(url, data, action);
				return true;
			} else {
				Utils.err("Network manager cannot connect, cannot access: " + url);
			}
		} else {
			Utils.err("No network manager set, cannot access: " + url);
		}
		return false;
	}
	
	
	
	
	public static void setNetworkManager(GLNetwork nw){
		if(GLNetworkManager.nwork == null){
			GLNetworkManager.nwork = nw;
		} 
	}
	
	public static GLNetwork getNetworkManager(){
		if(nwork == null){
			nwork = new GLNetworkNavigate();
			Utils.warn("Network manager not set! Initializing one now...");
		}
		return nwork;
	}

	public static void setMaximumNetworkFailures(int maximumNetworkFailures) {
		GLNetworkManager.maximumNetworkFailures = maximumNetworkFailures;
	}

	public static int getMaximumNetworkFailures() {
		return maximumNetworkFailures;
	}

	public static void setUrlTimeout(int urlTimeout) {
		GLNetworkManager.urlTimeout = urlTimeout;
	}

	public static int getUrlTimeout() {
		return urlTimeout;
	}
	
	public static void incrementNetworkFailure(){
		networkFailures++;
	}

	public static void setNetworkFailures(int networkFailures) {
		GLNetworkManager.networkFailures = networkFailures;
	}
	
	private static void updateNetworkFailures(){
		if(networkFailures > maximumNetworkFailures){
			setEnabled(false);
		}
	}
	
	public static int getNetworkFailures() {
		return networkFailures;
	}

	public static void setEnabled(boolean enabled) {
		GLNetworkManager.enabled = enabled;
	}

	public static boolean isEnabled() {
		return enabled;
	}
	
}
