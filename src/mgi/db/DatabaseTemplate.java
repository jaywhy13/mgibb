package mgi.db;

import java.util.Hashtable;
import java.util.Vector;

public abstract class DatabaseTemplate {
	
	/**
	 * Create the database and fire DbManagerEvent.DATABASE_CREATED when through. 
	 * @return - result of the creation exercise
	 */
	public abstract boolean createDatabase();
	
	public abstract boolean openDatabase();
	
	public abstract boolean deleteDatabase();
	
	public abstract boolean isDatabaseCreated();
	
	public abstract boolean isDatabaseOpened();
	
	public abstract boolean isDatabaseReady();
	
	/**
	 * Insert an object into the database
	 * @param d
	 * @return
	 */
	public abstract boolean insert(Databaseable d);
	
	/**
	 * Updates an object already inserted
	 * @param d
	 * @return
	 */
	public abstract boolean update(Databaseable d);
	
	/**
	 * Checks if the current object exists in the database
	 * @param d
	 * @return
	 */
	public abstract boolean exists(Databaseable d);
	
	/**
	 * Checks if the current object with the following conditions exists in the database
	 * @param tableName
	 * @param conditions
	 * @return
	 */
	public abstract boolean exists(String tableName, Hashtable conditions);
	
	public abstract boolean exists(String tableName, int id);
	
	public abstract boolean exists(Databaseable d, int id);
	
	/**
	 * Returns a Databaseable object by the table name and primary key
	 * @param id
	 * @return
	 */
	public abstract Databaseable select(String tableName, int id);
	
	public abstract Vector select(String tableName, Hashtable conditions);
	
	public abstract Vector select(Databaseable d, Hashtable conditions);
	
	
	/**
	 * Returns the number of objects of that type in the database
	 * @param tableName
	 * @return
	 */
	public abstract int count(String tableName);
	
	/**
	 * Returns the number of object of that type in the database
	 * @param d
	 * @return
	 */
	public abstract int count(Databaseable d);
	
	
	public abstract int count(String tableName, Hashtable conditions);
	
	
	public abstract boolean delete(String tableName, int id);
	
	public abstract boolean delete(String tableName, Hashtable conditions);
	
	public abstract boolean delete(Databaseable d);
	
	public abstract boolean beginTransaction();
	
	public abstract boolean commit();
	
	public abstract boolean rollback();
	
	public abstract boolean close();
	
	public abstract int max(Databaseable d);

	public abstract int max(String tableName);

	public abstract int max(String tableName, String columnName);
	
	public abstract int getNextId(String tableName);

	
	
}
