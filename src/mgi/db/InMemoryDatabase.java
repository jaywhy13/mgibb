package mgi.db;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import mgi.db.schema.ForeignSchemaField;
import mgi.db.schema.SchemaDefinition;
import mgi.events.GLEvents;
import mgi.util.Utils;

public class InMemoryDatabase extends DatabaseTemplate {

	private Hashtable database;
	private boolean created = false;

	public boolean createDatabase() {
		if (!created) {
			database = new Hashtable();
			created = true;
			GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_CREATED);
		}
		return true;
	}

	public static String getHashKey(Databaseable d) {
		return d.getTableName() + "_" + d.getId();
	}

	public boolean insert(Databaseable d) {
		if (!isDatabaseCreated()) {
			createDatabase();
		}

		String key = getHashKey(d);
		database.put(key, d);
		return insertRelated(d);
	}
	
	/**
	 * Inserts all the one to many relations into the database
	 * @param d
	 * @return
	 */
	public boolean insertRelated(Databaseable d){
		// We also need to manually insert the related ones
		SchemaDefinition schema = d.getSchema();
		if(schema != null){
			Vector oneToManyFields = schema.getOneToManyFields();
			Hashtable data = d.getData();
			for(int i = 0; i < oneToManyFields.size(); i++){
				ForeignSchemaField fkey = (ForeignSchemaField) oneToManyFields.elementAt(i);
				Vector relatedItems = Utils.getValue(data, fkey.getRelatedName(), new Vector());
				for(int j = 0; j < relatedItems.size(); j++){
					Databaseable relatedItem = (Databaseable) relatedItems.elementAt(j);
					if(!insert(relatedItem)){
						return false;
					}
				}
			}
		}
		return true;
	}

	public boolean exists(Databaseable d) {
		if (!isDatabaseCreated()) {
			return false;
		}
		String key = getHashKey(d);
		return database.containsKey(key);
	}

	public boolean exists(String tableName, int id) {
		String key = tableName + "_" + id;
		return database.containsKey(key);
	}

	public Databaseable select(String tableName, int id) {
		if (exists(tableName, id)) {
			String key = tableName + "_" + id;
			return (Databaseable) database.get(key);
		}
		return null;
	}

	public boolean delete(String tableName, int id) {
		String key = tableName + "_" + id;
		Object o = database.remove(key);
		if (o != null) {
			return true;
		}
		// nothing was removed
		return false;
	}
	
	public boolean delete(String tableName, Hashtable conditions) {
		// nothing was removed
		return false;
	}

	

	public boolean update(Databaseable d) {
		return insert(d);
	}

	public boolean isDatabaseCreated() {
		return created;
	}

	public int count(String tableName) {
		int total = 0;
		Enumeration keys = database.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			if (key.startsWith(tableName + "_")) {
				total++;
			}
		}
		return total;
	}

	public int count(String tableName, Hashtable conditions) {
		Vector results = select(tableName, conditions);
		if (results != null) {
			return results.size();
		}

		return 0;
	}

	public int count(Databaseable d) {
		return count(d.getTableName());
	}

	public boolean delete(Databaseable d) {
		// TODO Auto-generated method stub
		String key = getHashKey(d);
		if (database.containsKey(key)) {
			database.remove(key);
		}
		return false;
	}

	public boolean beginTransaction() {
		return false;
	}

	public boolean commit() {
		return false;
	}

	public boolean rollback() {
		return false;
	}

	public boolean openDatabase() {
		return createDatabase();
	}

	public boolean isDatabaseOpened() {
		return openDatabase();
	}

	public boolean isDatabaseReady() {
		return openDatabase();
	}

	public Vector select(String tableName, Hashtable conditions) {
		Vector results = new Vector();
		Utils.debug("am ere");
		Enumeration keys = database.keys();
		Databaseable d = Databaseable.createFromData(tableName, conditions);
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			if (key.startsWith(tableName + "_")) {
				Databaseable d2 = (Databaseable) database.get(key);
				if (d.like(d2)) {
					results.addElement(d2);
				}
			}
		}
		return results;
	}

	public Vector select(Databaseable d, Hashtable conditions) {
		return select(d.getTableName(), conditions);
	}

	public boolean exists(String tableName, Hashtable conditions) {
		return select(tableName, conditions).size() >= 1;
	}

	public boolean exists(Databaseable d, int id) {
		return exists(d.getTableName(), id);
	}

	public boolean deleteDatabase() {
		database = null;
		created = false;
		return true;
	}
	
	public boolean close() {
		return true;
	}

	public int max(Databaseable d) {
		return 0;
	}

	public int max(String tableName) {
		return 0;
	}

	public int max(String tableName, String columnName) {
		return 0;
	}

	public int getNextId(String tableName) {
		return 0;
	}

	
	

}
