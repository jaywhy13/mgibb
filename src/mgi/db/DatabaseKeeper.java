package mgi.db;

import java.util.Enumeration;
import java.util.Hashtable;

import net.rim.device.api.database.DataTypeException;

import mgi.events.GLEvents;
import mgi.util.Utils;

public class DatabaseKeeper {

	public static final int DATABASE_SQLITE = 1;
	public static final int DATABASE_IN_MEMORY = 2;
	public static final String DEFAULT_DATABASE_NAME = "keeper.db";

	private static Hashtable databases = new Hashtable();
	private static Hashtable databaseTypes = new Hashtable();
	
	private static String databaseLocation;

	public static String getDatabaseLocation() {
		if(databaseLocation == null){
			databaseLocation = "file:///SDCard/";
		}
		
		// make sure it ends with a trailing slash
		if(!databaseLocation.endsWith("/")){
			databaseLocation += "/";
		}
		
		return databaseLocation;
	}

	public static void setDatabaseLocation(String databaseLocation) {
		DatabaseKeeper.databaseLocation = databaseLocation;
	}

	public static boolean isDatabaseTypeSupported(int databaseType) {
		return false;
	}

	/**
	 * Returns true if the database is up, ready and waiting for data
	 * 
	 * @return
	 */
	public static boolean isDatabaseReady(String databaseName) {
		DatabaseTemplate db = getDatabase(databaseName);
		if (db != null) {
			return ((DatabaseTemplate) db).isDatabaseReady();
		}
		return false;
	}

	public static boolean isDatabaseReady() {
		return isDatabaseReady(DEFAULT_DATABASE_NAME);
	}

	/**
	 * Creates or opens database given a type
	 * @param databaseType
	 * @return
	 */
	public static boolean createDatabase(int databaseType) {
		return createDatabase(DEFAULT_DATABASE_NAME, databaseType);
	}

	public static boolean createDatabase(String databaseName, int databaseType) {
		DatabaseTemplate database = null;
		if (databaseType == DATABASE_SQLITE) {
			database = new SQLiteDatabase(databaseName);
		} else if (databaseType == DATABASE_IN_MEMORY) {
			database = new InMemoryDatabase();
		} else {
			return false;
		}
		
		Hashtable dict = new Hashtable();
		dict.put("databaseName", databaseName);
		dict.put("database", database);
		

		// Insert the name and the type of db
		databaseTypes.put(databaseName, new Integer(databaseType));
		databases.put(databaseName, database);
		
		
		// create the database if we need to
		Utils.log("Keeper attempting to create database: " + databaseName);
		if(!database.isDatabaseCreated()){
			if(!database.createDatabase()){
				GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_LOAD_FAILURE, dict);
				return false;
			} 
		}
		
		// try to open the database
		Utils.log("Keeper trying to open database");
		if(!database.openDatabase()){
			GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_LOAD_FAILURE, dict);
			return false;
		} 
		

		
		// check if the database is ready
		Utils.log("Keeper checking if database is ready");
		if(database.isDatabaseReady()){
			Utils.log("Keeper is happy, the database is open and ready :)");
			GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_READY, dict);
			if(!isDatabaseReady(databaseName)){
				Utils.err("Summin's wrong... someone's lying!");
				return false;
			}
			return true;
		} else {
			GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_LOAD_FAILURE, dict);
		}
		return false;
	}
	
	/**
	 * Returns the type of the default database
	 * @return
	 */
	public static int getDatabaseType(){
		return getDatabaseType(DEFAULT_DATABASE_NAME);
	}

	public static int getDatabaseType(String databaseName) {
		if(databaseTypes.containsKey(databaseName)){
			return ((Integer)databaseTypes.get(databaseName)).intValue();
		}
		return -1;
	}

	public static DatabaseTemplate getDatabase() {
		return getDatabase(DEFAULT_DATABASE_NAME);
	}

	public static DatabaseTemplate getDatabase(String databaseName) {
		if (databases.containsKey(databaseName)) {
			return (DatabaseTemplate) databases.get(databaseName);
		}
		return null;
	}
	
	public static void closeDatabases(){
		Enumeration databaseNames = databases.keys();
		while(databaseNames.hasMoreElements()){
			String databaseName = databaseNames.nextElement().toString();
			DatabaseTemplate database = (DatabaseTemplate) databases.get(databaseName);
			database.close();
		}
	}

}
