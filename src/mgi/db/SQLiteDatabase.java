package mgi.db;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import mgi.db.schema.ForeignSchemaField;
import mgi.db.schema.SchemaDefinition;
import mgi.events.GLEvents;
import mgi.events.GLPrerequisites;
import mgi.util.Utils;
import net.rim.device.api.database.Cursor;
import net.rim.device.api.database.DataTypeException;
import net.rim.device.api.database.Database;
import net.rim.device.api.database.DatabaseException;
import net.rim.device.api.database.DatabaseFactory;
import net.rim.device.api.database.DatabaseIOException;
import net.rim.device.api.database.DatabasePathException;
import net.rim.device.api.database.Row;
import net.rim.device.api.database.Statement;
import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.MalformedURIException;
import net.rim.device.api.io.URI;
import net.rim.device.api.system.ControlledAccessException;

public class SQLiteDatabase extends DatabaseTemplate {

	public SQLiteDatabase(String databaseName) {
		this.databaseName = databaseName;
	}

	/**
	 * Used to keep a list of schema file location that should be auto
	 * installed. Use addSchemaLocation to add a schema file.
	 */
	private Vector schemaFileLocations = new Vector();

	private boolean firstLoad = false;

	private String databaseName;

	public boolean dbSupported = true;

	private boolean open = false;

	private Database database;
	
	/**
	 * This keeps a local copy of ids that can be used for ids. 
	 * Apparently the version of SQL Lite we're using doesn't 
	 * have a SEQUENCE table so this is our work around. The max method
	 * will go to the database the first time to get the value, but
	 * once we're in the program we'll rely on this local cache to 
	 * give us IDs
	 */
	private Hashtable nextSequenceNumbers;

	/**
	 * Returns true if the database has just been created
	 * 
	 * @return
	 */
	public boolean isFirstLoad() {
		return firstLoad;
	}

	/**
	 * Returns true if the database has already been created. The database is
	 * stored on the SD card.
	 * 
	 * @return
	 */
	public boolean isDatabaseCreated() {
		if (!dbSupported)
			return false;

		try {
			String root = Utils.isSDCardInserted() ? "file:///SDCard/" : "file:///store/"; 
			FileConnection fc = (FileConnection) Connector
					.open(root + databaseName);
			boolean exists = fc.exists();
			fc.close();
			return exists;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Opens a connection to the database and returns a boolean.
	 * 
	 * @return
	 */
	public boolean openDatabase() {
		Utils.log("Asked to open database: " + databaseName);
		GLPrerequisites
				.prerequisiteStarted(GLPrerequisites.PREREQUISITE_IN_MEMORY_DATABASE_READY);

		if (!dbSupported) {
			Utils.err("Database is not supported, cannot open database");
			GLPrerequisites
					.prerequisiteFailed(GLPrerequisites.PREREQUISITE_IN_MEMORY_DATABASE_READY);
			return false;
		}

		if (!isDatabaseCreated()) {
			Utils.log("Database does not exist, cannot open. Will attempt to create");
			if (!createDatabase()) {
				Utils.err("Database creation failed, cannot open database");
				GLPrerequisites
						.prerequisiteFailed(GLPrerequisites.PREREQUISITE_IN_MEMORY_DATABASE_READY);
				return false;
			}
		}

		if (isDatabaseOpened()) {
			Utils.log("Database exists, no need to open... returning");
			GLPrerequisites
					.prerequisiteComplete(GLPrerequisites.PREREQUISITE_LOCAL_DATABASE_READY);
			return true;
		}

		if (isDatabaseCreated()) {
			URI dbURI = getDbURI(databaseName);
			if (dbURI == null) {
				Utils.err("Null database URI received, cannot open database");
				GLPrerequisites
						.prerequisiteFailed(GLPrerequisites.PREREQUISITE_LOCAL_DATABASE_READY);
				return false;
			}
			try {
				Utils.log("Attempting to open database: " + databaseName);
				database = DatabaseFactory.open(dbURI);
				Utils.log("Database successfully opened");
				open = true;
				// Notify all listeners
				GLEvents.triggerEvent(GLEvents.EVENT_DATABASE_OPENED);
				GLPrerequisites
						.prerequisiteComplete(GLPrerequisites.PREREQUISITE_LOCAL_DATABASE_READY);
				return true;
			} catch (ControlledAccessException e) {
				Utils.err("CAException: " + e.getMessage());
			} catch (DatabaseIOException e) {
				Utils.err("DbIOException: " + e.getMessage());
				// If we get error 12, this means more than 1 connection to the
				// database
			} catch (DatabasePathException e) {
				Utils.err("DbPException: " + e.getMessage());
				dbSupported = false;
				Utils.log("Disabling DB for app");
			}
		}
		GLPrerequisites
				.prerequisiteFailed(GLPrerequisites.PREREQUISITE_LOCAL_DATABASE_READY);
		return false;
	}

	/**
	 * Returns the database, if the database exists.
	 * 
	 * @return
	 */
	public Database getDatabase() {
		// Utils.log("Asked to get: " + databaseName);
		if (!dbSupported) {
			System.out
					.println("GL [II] Database is not supported, cannot get database");
			return null;
		}

		if (isDatabaseCreated() && isDatabaseOpened()) {
			// Utils.log("Database is created and opened... returning it");
			return database;
		} else {
			if (!isDatabaseCreated()) {
				Utils.err(databaseName + " is not created");
			}

			if (!isDatabaseOpened()) {
				Utils.err(databaseName + " is not opened");
			}
		}

		if (database == null) {
			Utils.err("Database is null, cannot return it");
		}

		return null;
	}

	/**
	 * Returns a URI for connecting to the database
	 * 
	 * @return
	 */
	private static URI getDbURI(String databaseName) {

		URI dbURI = null;
		try {
			dbURI = URI.create(DatabaseKeeper.getDatabaseLocation() + databaseName);
		} catch (IllegalArgumentException e) {
			Utils.err("IAException: " + e.getMessage());
		} catch (MalformedURIException e) {
			Utils.err("MURIException: " + e.getMessage());
		}
		return dbURI;
	}

	/**
	 * Reads a text file from inside the package structure and returns a string
	 * with the contents. Method returns null if we cannot find the text file.
	 * 
	 * @param fileName
	 * @return
	 */
	private static String readTextFileFromPkg(String fileName) {
		String result = null;
		try {
			Class classs = Class.forName("glo.db.GasDb");
			InputStream is = classs.getResourceAsStream(fileName);
			if (is != null) {
				byte[] data = IOUtilities.streamToBytes(is);
				result = new String(data);
			}
		} catch (ClassNotFoundException e) {
			Utils.err("CNFException: " + e.getMessage());
		} catch (IOException ioe) {
			Utils.err("CNFException: " + ioe.getMessage());
		}
		return result;
	}

	/**
	 * Installs schema from a text file in the package. NB: Only one table can
	 * be created at a time. Store one table per file.
	 * 
	 * @param d
	 * @param fileLocation
	 * @return
	 */
	private boolean installSchemaFromPackageFile(String fileLocation) {
		if (isDatabaseOpened()) {
			Utils.err("No connection to the database is currently open");
			return false;
		}
		String schemaSql = readTextFileFromPkg(fileLocation);
		if (schemaSql != null) {
			Utils.log("Installing schema: \n" + schemaSql);
			try {
				Statement stmt = database.createStatement(schemaSql);
				stmt.prepare();
				stmt.execute();
				Utils.log("Schema installed from " + fileLocation);
				return true;

			} catch (DatabaseException e) {
				Utils.err("Error creating database schema");
				Utils.err("DException: " + e.getMessage());
				return false;
			}
		}
		return false;
	}

	/**
	 * Creates the database at the URI location.
	 * 
	 * @return
	 */
	public boolean createDatabase() {
		Utils.log("Asked to create SQLite database: " + databaseName);
		if (isDatabaseCreated()) {
			// Utils.log("Database already exists, exiting");
			return true;
		}

		URI dbURI = getDbURI(databaseName);
		Utils.log("No database exists, will create " + databaseName);
		if (dbURI != null) {
			try {
				Utils.log("[SQLite] Opening or creating db at: " + dbURI);
				Database d = DatabaseFactory.openOrCreate(dbURI);
				if (d != null) {
					Utils.log("Created SQLite db: " + dbURI);
					firstLoad = true;
					database = d;
					open = true;
					Utils.log("Database successfully created");
					return true;
				}
			} catch (DatabaseIOException e) {
				Utils.err("DIOException: " + e.getMessage());
			} catch (DatabasePathException e) {
				Utils.err("DPException:  " + e.getMessage());
				dbSupported = false;
				Utils.log("Disabling DB for app");
			} catch (Exception e) {
				Utils.err("Could not create database: " + e.getMessage());
				e.printStackTrace();
				dbSupported = false;
			}
		}
		return false;
	}

	/**
	 * Tells whether the database is open
	 * 
	 * @return
	 */
	public boolean isDatabaseOpened() {
		return open && dbSupported;
	}

	/**
	 * Returns true if the database is supported
	 * 
	 * @return
	 */
	public boolean isDbSupported() {
		return dbSupported;
	}

	/**
	 * Adds a schema location to the list of files that should be parsed for
	 * database creation
	 * 
	 * @param fileLocation
	 */
	public void addSchemaFileLocation(String fileLocation) {
		schemaFileLocations.addElement(fileLocation);
	}

	/**
	 * Returns the schema locations as an array of strings
	 * 
	 * @return
	 */
	public String[] getSchemaFileLocationsAsArray() {
		String[] locations = new String[schemaFileLocations.size()];
		for (int i = 0; i < schemaFileLocations.size(); i++) {
			locations[i] = schemaFileLocations.elementAt(i).toString();
		}
		return locations;
	}

	// =============================================
	// Code for interaction with the database
	public void runTests() {
		Utils.log("Running tests");
		Hashtable data = new Hashtable();
		data.put("id", new Integer(-1));
		data.put("name", "MGI Gas Station");

		beginTransaction();
		if (insert("GStation", data)) {
			Utils.log("Test insertion succeeded");
			// Now try the select
			// Cursor results = select("GStation", -1);
			// if (results == null) {
			// Utils.err("Null result retrieved from query");
			// } else {
			// try {
			// if (!results.isEmpty()) {
			// if(results.first()){
			// Row r = results.getRow();
			// Utils.log("Retrieved row: " + r.toString());
			// } else {
			// Utils.err("Could not advance cursor to first row");
			// }
			// } else {
			// Utils.err("Result set is empty");
			// }
			// } catch (DatabaseException e) {
			// Utils.err("Database exception occurred while attempting select on test data.\nReason: "
			// + e.getMessage());
			// }
			// }

		} else {
			System.out
					.println("GL [EE] Tests failed, database possibly not ready");
		}
		commitTransaction();
		// rollbackTransaction();
	}

	public Cursor getTableData(String tblName) {
		Cursor results = cursorSelect(tblName);
		if (results == null) {
			System.out
					.println("GL [EE] Null result retrieved from querying the table "
							+ tblName);
			return null;
		} else {
			try {
				if (!results.isEmpty()) {
					return results;
				} else {
					Utils.err("Result set from the table " + tblName
							+ " is empty");
					return null;
				}
			} catch (DatabaseException e) {
				System.out
						.println("GL [EE] Database exception occurred while attempting select on table"
								+ tblName + ".\nReason: " + e.getMessage());
				return null;
			}
		}
	}

	public static String sqlCleanString(String str) {
		String newString = str.replace('\"', ' ');
		return newString;
	}

	protected static String[] getColumnsAndValueString(Hashtable data) {
		return getColumnsAndValueString(data, false, ",");
	}

	/**
	 * Returns columns and values string excluding foreign key values (Vector)
	 * 
	 * @param data
	 * @param useEquals
	 * @param valueDelimiter
	 * @return
	 */
	protected static String[] getColumnsAndValueString(Hashtable data,
			boolean useEquals, String valueDelimiter) {

		// declare vectors to store values and cols
		Vector cols = new Vector();
		Vector values = new Vector();

		Enumeration keys = data.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			if (data.get(key).getClass() == Vector.class) {
				continue; // skip one to many relations
			}
			String value = data.get(key).toString();
			cols.addElement(key);
			if (useEquals) {
				values.addElement(key + " = '" + value + "'");
			} else {
				values.addElement("\"" + value + "\"");
			}
		}

		String[] result = new String[2];
		result[0] = Utils.join(cols, ", ");
		result[1] = Utils.join(values, valueDelimiter + " ");
		return result;
	}

	private String getHashtableAsWhereClause(Hashtable data) {
		if (data == null) {
			return "";
		}

		// get the total of non-vector values
		int oneToManyRelations = 0;
		Enumeration keys = data.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			if (data.get(key).getClass() == Vector.class) {
				oneToManyRelations++;
			}
		}

		String[] conditions = new String[data.size() - oneToManyRelations];
		Enumeration e = data.keys();
		int index = 0;
		while (e.hasMoreElements()) {
			String key = e.nextElement().toString();
			String value = "";

			Object obj = data.get(key);
			if (obj.getClass() == Vector.class) {
				continue; // skip Vector
			}

			if (data.get(key) != null) {
				value = data.get(key).toString();
			}
			value = value.trim();

			if (value.equals("")) {
				continue;
			}

			char firstChar = value.charAt(0);

			String clause = "";
			if (firstChar == '>' || firstChar == '<'
					|| value.startsWith("LIKE")) {
				Utils.warn("Not quoting value: " + value);
				clause = key + "" + value;
			} else {
				value = "'" + value + "'";
				clause = key + "=" + value;
			}

			conditions[index] = clause;
			index++;
		}

		if (conditions.length > 0) {
			return " WHERE " + Utils.join(conditions, " AND ");
		}

		return "";
	}

	public int count(String tableName, Hashtable criteria) {
		String sql = "SELECT COUNT(*) FROM " + tableName;
		sql += getHashtableAsWhereClause(criteria);
		try {
			Statement stmt = getDatabase().createStatement(sql);
			stmt.prepare();
			Cursor c = stmt.getCursor();
			if (c != null) {
				int actualCount = 0;
				if (!c.isEmpty()) {
					c.first();
					Row row = c.getRow();
					actualCount = row.getInteger(0);
				}
				// Utils.log("[Count=" + actualCount + "] SQL: " + sql);
				return actualCount;

			} else {
				Utils.err("Got back a null cursor");
				return 0;
			}
		} catch (DatabaseException e) {
			System.out
					.println("GL [EE] Database exception occurred while trying to select. \nReason: "
							+ e.getMessage());
		} catch (NullPointerException npe) {
			System.out
					.println("GL [EE] Database not ready, cannot perform select for: "
							+ tableName);
		} catch (DataTypeException e) {
			System.out
					.println("GL [EE] Database exception while trying to get count for: "
							+ tableName + ".\nReason: " + e.getMessage());
		}
		return 0;

	}

	/**
	 * Tells us whether a table is empty
	 * 
	 * @param tableName
	 * @return
	 */
	public boolean isEmpty(String tableName) {
		try {
			Cursor c = cursorSelect(tableName);
			return c.isEmpty();
		} catch (DatabaseException e) {
			return true;
		}
	}

	public Vector select(String tableName, Hashtable data) {
		if (!createTable(tableName)) {
			Utils.err(tableName
					+ " does not exist and we could not create it! Cannot select");
			return null;
		}

		Cursor cursor = cursorSelect(tableName, data);
		if (cursor != null) {
			Vector results;
			try {
				results = getDatabaseables(tableName, cursor);
				return results;
			} catch (DatabaseException e) {
				Utils.err("Database exception occurred while trying to select. \nReason: "
						+ e.getMessage());
			}

		} else {
			Utils.err("Got a null cursor trying to select from " + tableName
					+ " " + getHashtableAsWhereClause(data));
		}
		return null;
	}

	public Cursor cursorSelect(String tableName) {
		return cursorSelect(tableName, null);
	}

	public Cursor cursorSelect(String tableName, Hashtable conditions) {
		String whereClause = getHashtableAsWhereClause(conditions);

		String sql = "SELECT * FROM " + tableName + " " + whereClause;
		try {
			Statement stmt = getDatabase().createStatement(sql);
			stmt.prepare();
			return stmt.getCursor();
		} catch (DatabaseException e) {
			Utils.err("Database exception occurred while trying to select. \nReason: "
					+ e.getMessage());
		} catch (NullPointerException npe) {
			System.out
					.println("GL [EE] Database not ready, cannot perform select for: "
							+ tableName);
		}
		return null;
	}

	public Databaseable select(String tableName, int id) {
		Hashtable data = new Hashtable();
		data.put("id", new Integer(id));
		Vector results = select(tableName, data);
		if (results != null) {
			if (results.size() > 0) {
				return (Databaseable) results.elementAt(0);
			}
		}
		return null;
	}

	public Vector getDatabaseables(String tableName, Cursor cursor)
			throws DatabaseException {
		Vector results = new Vector();

		while (cursor.next()) {
			SchemaDefinition schema = Databaseable
					.getSchemaForTableName(tableName);
			Row row = cursor.getRow();
			Hashtable data = new Hashtable();
			String[] columnNames = row.getColumnNames();
			// Put everything in the hash table
			for (int i = 0; i < columnNames.length; i++) {
				String columnName = columnNames[i];
				int columnIndex = row.getColumnIndex(columnName);
				String value = "";
				// Try saving a String
				try {
					Object obj = row.getObject(columnIndex);
					if (obj != null) {
						value = obj.toString();
						Utils.put(data, columnName, value);
					}
				} catch (DataTypeException e) {
					Utils.warn(columnName + " could not get a value for "
							+ tableName + "." + columnName);
				}
			}

			Databaseable d = Databaseable.fromTableName(tableName);
			if (d != null) {
				String id = data.get("id").toString();

				// check if we have foreign keys
				if (schema != null) {
					if (schema.hasOneToManyFields()) {
						Vector foreignKeys = schema.getOneToManyFields();
						for (int k = 0; k < foreignKeys.size(); k++) {
							ForeignSchemaField fkey = (ForeignSchemaField) foreignKeys
									.elementAt(k);
							String relatedTableName = fkey.getForeignTable();
							String nameOnRelated = fkey
									.getColumnNameOnRelatedSchema();
							if (nameOnRelated != null) {
								Hashtable relatedCriteria = new Hashtable();
								relatedCriteria.put(nameOnRelated, id);
								Vector relatedItems = select(relatedTableName,
										relatedCriteria);
								Utils.put(data, fkey.getName(), relatedItems,
										new Vector());
							} else {
								Utils.err("Could not get related name for "
										+ fkey);
							}
						}
					}
				} else {
					Utils.warn("No schema available for " + tableName
							+ ", cannot attempt SELECT for related items");
				}
				results.addElement(d.fromHashtable(data));

			} else {
				Utils.err("Could not create databaseable from tableName "
						+ tableName);
			}
		}
		return results;
	}
	

	public int max(Databaseable d) {
		return max(d.getTableName());
	}

	public int max(String tableName) {
		return max(tableName, "id");
	}

	
	public int max(String tableName, String columnName) {
		String key = tableName + "." + columnName;
		if(nextSequenceNumbers.containsKey(key)){
			// Take the current value of the sequence if we're already tracking it...
			int value = ((Integer) nextSequenceNumbers.get(key)).intValue();
			return value;
		} else {
//			Utils.log("[SQL] nextSequenceNumbers has no key for " + key + ", going to db");
			String sql = "SELECT max(" + columnName + ") FROM " + tableName;
			try {
				Statement stmt = getDatabase().createStatement(sql);
				stmt.prepare();
				Cursor cursor = stmt.getCursor();
				Vector results = getDatabaseables(tableName, cursor);
				if (results.size() > 0) {
					Databaseable d = (Databaseable) results.elementAt(0);
					int maxValue = d.getId();
					// Store the max value in the sequence
					setSequenceNumber(tableName, columnName, maxValue);
					return maxValue;
				}
			} catch (DatabaseException e) {
				Utils.warn("Database exception while trying max. " + e.getMessage());
			} catch (NullPointerException npe) {
				Utils.warn("Null pointer exception while trying max" + tableName);
			}
			// Assume the table is not yet created, so just return 0 as the max value 
			setSequenceNumber(tableName, columnName, 0);
			return 0;
		}
	}

	public boolean insert(String tableName, Hashtable data) {

		if (!createTable(tableName)) {
			Utils.err(tableName + " does not exist and we could not create it!");
			return false;
		}

		int id;
		try {
			id = Integer.valueOf(data.get("id").toString()).intValue();
		} catch (Exception e) {
			Utils.err("Cannot insert into " + tableName + " without an ID.");
			return false;
		}

		if (exists(tableName, id)) {
			return update(tableName, data);
		}

		if (data == null) {
			Utils.err("Cannot insert null data into " + tableName);
			return false;
		} else {
			if (data.size() == 0) {
				System.out
						.println("GL [EE] No data for insertion, cannot insert.");
				return false;
			}
		}

		if (tableName == null) {
			System.out
					.println("GL [EE] No table name provided, no insertions will be done");
			return false;
		}

		boolean result = false;
		String sql = "INSERT INTO " + tableName + " ( ";
		String[] colAndStrs = getColumnsAndValueString(data);
		String colStr = colAndStrs[0];
		String valueStr = colAndStrs[1];

		sql += colStr + ") VALUES ( " + valueStr + " )";
		result = exec(sql);
		
		if(!result){
			Utils.err("Failed to execute: " + sql);
			Utils.printHashTable(data);
		}

		if (result) {
			boolean relatedSuccess = insertRelated(tableName, data);
			if(!relatedSuccess){
				Utils.err("Failed to insert related for: " + tableName);
				Utils.printHashTable(data);
			}
			return relatedSuccess;
		}
		return false;
	}

	public boolean insertRelated(String tableName, Hashtable data) {
		// Check if we have any one-to-many relationships, loop and insert
		// these... we need a schema first
		SchemaDefinition schema = Databaseable.getSchemaForTableName(tableName);
		if (schema != null) {
			if (schema.getOneToManyFields().size() > 0) {
				Vector foreignKeys = schema.getOneToManyFields();
				for (int i = 0; i < foreignKeys.size(); i++) {
					ForeignSchemaField foreignKey = (ForeignSchemaField) foreignKeys
							.elementAt(i);
					String relatedName = foreignKey.getRelatedName();
					String relatedTableName = foreignKey.getForeignTable();
					Vector relatedItems = Utils.getValue(data, relatedName,
							new Vector());
//					Utils.log(relatedItems.size() + " " + relatedName
//							+ " to insert for " + tableName);
					if (relatedItems.size() > 0) {
						for (int r = 0; r < relatedItems.size(); r++) {
							Databaseable relatedItem = (Databaseable) relatedItems
									.elementAt(r);
							if (!insert(relatedTableName, relatedItem.getData())) {
								return false;
							}
						}
					}
				}
//				Utils.log("Done inserting related stuff (we think) for "
//						+ tableName);
				return true; // if we got this far... return true

			} else {
//				Utils.log(tableName + " has no foreign key relationships");
				return true;
			}
		} else {
			Utils.warn("No schema available for " + tableName
					+ ", cannot check for relationships");
			return true; // return true if we can't find a schema
		}
	}

	public boolean update(String tableName, Hashtable data) {
		if (data == null) {
			Utils.err("Cannot update null data in " + tableName);
			return false;
		} else {
			if (data.size() == 0) {
				Utils.err("No data for updating, cannot update.");
				return false;
			}

			if (data.get("id") == null) {
				Utils.warn("No id in data for updating, cannot update.");
				return false;
			}

		}

		if (tableName == null) {
			System.out
					.println("GL [EE] No table name provided, no updates will be done");
			return false;
		}

		if (!createTable(tableName)) {
			Utils.err(tableName + " does not exist and we could not update it!");
			return false;
		}

		boolean result = false;

		String sql = "UPDATE " + tableName + " SET ";

		String[] colAndStrs = getColumnsAndValueString(data, true, ",");
		String valueStr = colAndStrs[1];

		sql += valueStr;
		sql += " WHERE id=" + data.get("id").toString();

		result = exec(sql);

		if (result) {
			return insertRelated(tableName, data);
		}
		return false;
	}
	
	/**
	 * Prepares the data for insertion or updates... primarily, we remove any -1 
	 * ids and update the related tables. 
	 * @param tableName
	 * @param data
	 */
	protected void prepareData(String tableName, Hashtable data){
//		int id = Utils.getValue(data, "id")
	}

	public boolean remove(String tableName, Hashtable data) {
		if (tableName == null) {
			System.out
					.println("GL [EE] No table name provided, no removals will be done");
			return false;
		}

		if (!createTable(tableName)) {
			Utils.err(tableName
					+ " does not exist and we could not create it! Cannot delete!");
			return false;
		}

		boolean result = false;
		String sql = "DELETE FROM " + tableName;

		if (data != null) {
			String[] colAndStrs = getColumnsAndValueString(data, true, "AND");
			String colStr = colAndStrs[0];
			String valueStr = colAndStrs[1];

			sql += " WHERE  " + valueStr + " ";
		}

		result = exec(sql);
		return result;
	}

	private boolean exec(String sql) {
		boolean result = false;
		if (isDatabaseOpened()) {
			try {
				Database d = getDatabase();
				if (d != null) {
					// Utils.log("Preparing to execute sql: " + sql);
					Statement stmt = d.createStatement(sql);
					stmt.prepare();
					// Utils.log("Executing....");
					stmt.execute();
					stmt.close();
					Utils.log("Executed SQL: " + sql);
					result = true;
				} else {
					Utils.err("Null database, cannot insert");
				}
			} catch (DatabaseException e) {
				Utils.err("Could not execute statement: " + sql + "\nReason: "
						+ e.getMessage());
				e.printStackTrace();
			}
		} else {
			Utils.err("Cannot exceute SQL, database not open");
		}
		return result;
	}

	/**
	 * Begins a transaction
	 * 
	 * @return
	 */
	public boolean beginTransaction() {
		boolean result = false;
		if (isDatabaseOpened()) {
			try {
				Utils.log("Beginning transaction");
				Database d = getDatabase();
				if (d != null) {
					d.beginTransaction();
					Utils.log("Transaction started");
					result = true;
				} else {
					Utils.err("Null database, cannot begin transaction");
				}
			} catch (DatabaseException e) {
				Utils.err("Error starting transaction. " + e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Commits the transaction
	 * 
	 * @return
	 */
	public boolean commitTransaction() {
		boolean result = false;
		if (isDatabaseOpened()) {
			Utils.log("Committing ....");
			Database d = getDatabase();
			if (d != null) {
				try {
					d.commitTransaction();
					Utils.log("Committed!");
					result = true;
				} catch (DatabaseException e) {
					Utils.err("Error attempting to commit... " + e.getMessage());
				}
			} else {
				Utils.err("Could not commit, null database");
			}
		} else {
			Utils.err("Database is not open, cannot commmit");
		}
		return result;
	}

	/**
	 * Rolls back the transaction
	 * 
	 * @return
	 */
	public boolean rollbackTransaction() {
		boolean result = false;
		if (isDatabaseOpened()) {
			Utils.log("Rolling back ....");
			Database d = getDatabase();
			if (d != null) {
				try {
					d.rollbackTransaction();
					Utils.log("Rolled back!");
					result = true;
				} catch (DatabaseException e) {
					Utils.err("Error attempting to rollback... "
							+ e.getMessage());
				}

			} else {
				Utils.err("Could not rollback, null database");
			}
		} else {
			Utils.err("Database is not open, cannot rollback");
		}
		return result;
	}

	public boolean isDatabaseReady() {
		return isDatabaseCreated() && isDatabaseOpened();
	}

	private boolean tableExists(Databaseable d) {
		return tableExists(d.getTableName());
	}

	private boolean tableExists(SchemaDefinition schema) {
		return tableExists(schema.getTableName());
	}

	public boolean tableExists(String tableName) {
		if (isDatabaseReady()) {
			Hashtable criteria = new Hashtable();
			// SELECT name FROM sqlite_master WHERE type='table' AND
			// name='table_name';
			// remove the "[]" from the table name
			// trim first
			tableName = tableName.trim();
			tableName = tableName.replace('[', ' ');
			tableName = tableName.replace(']', ' ');
			tableName = tableName.trim();

			criteria.put("type", "table");
			criteria.put("name", tableName);
			return count("sqlite_master", criteria) == 1;
		}
		return false;
	}

	private boolean createTable(Databaseable d) {
		return createTable(d.getSchema());
	}

	private boolean createTable(String tableName) {
		SchemaDefinition schema = Databaseable.getSchemaForTableName(tableName);
		if (schema != null) {
			return createTable(schema);
		}
		Utils.warn("No schema exists for " + tableName
				+ ". Cannot create table");
		return false;
	}

	private boolean createTable(SchemaDefinition schema) {
		if (schema != null) {
			if (tableExists(schema)) {
				// Utils.log(schema.getTableName()
				// + " already exists, no need to create it");
				return true;
			}
			String sql = schema.asSQL();
			// Utils.log("Creating table for: " + schema.getTableName() + "\n"
			// + sql);
			if (exec(sql)) {
				// Utils.log("Ok, we managed to create the table, now check again if it exists");
				// now check if the table exists
				boolean creationSuccess = tableExists(schema);
				if (!creationSuccess) {
					Utils.err("Failed to create " + schema.getTableName());
				}
				return creationSuccess;
			} else {
				Utils.err("Create table SQL failed for "
						+ schema.getTableName());
				return false;
			}
		} else {
			Utils.err("Passed NULL schema, cannot create table");
		}
		return false;
	}

	public boolean insert(Databaseable d) {
		if (createTable(d)) { // ensure the table exists...
			// set the id if none exists...
			if (d.getId() == -1) {
				d.setId(getNextId(d.getTableName()));
			}
			return insert(d.getTableName(), d.getData());
		} else {
			Utils.err("No such table exists and we cudn't create it");
		}
		return false;
	}

	public boolean update(Databaseable d) {
		return update(d.getTableName(), d.getData());
	}

	public int count(Databaseable d) {
		return count(d.getTableName());
	}

	public boolean delete(String tableName, int id) {
		Hashtable conditions = new Hashtable();
		conditions.put("id", new Integer(id));
		return remove(tableName, conditions);
	}
	
	public boolean delete(String tableName, Hashtable conditions) {
		return remove(tableName, conditions);
	}

	public boolean delete(Databaseable d) {
		return delete(d.getTableName(), d.getId());
	}

	public boolean commit() {
		return false;
	}

	public boolean rollback() {
		return false;
	}

	public int count(String tableName) {
		return count(tableName, null);
	}

	public Vector select(Databaseable d, Hashtable conditions) {
		return select(d.getTableName(), conditions);
	}

	public boolean exists(Databaseable d) {
		return exists(d.getTableName(), d.getData());
	}

	public boolean exists(String tableName, Hashtable conditions) {
		Vector results = select(tableName, conditions);
		if (results != null) {
			return results.size() == 1;
		}
		return false;
	}

	public boolean exists(String tableName, int id) {
		Hashtable idHash = new Hashtable();
		idHash.put("id", new Integer(id));
		return exists(tableName, idHash);
	}

	public boolean exists(Databaseable d, int id) {
		return exists(d.getTableName(), id);
	}

	/**
	 * Deletes the sqlite db file
	 */
	public boolean deleteDatabase() {
		try {
			database.close();
			URI dbURI = getDbURI(databaseName);
			DatabaseFactory.delete(dbURI);
			return true;
		} catch (DatabaseIOException e) {
			Utils.err("Error closing database");
		} catch (DatabasePathException e) {
			Utils.warn("Invalid path, unable to delete database");
		}
		return false;
	}

	public boolean isCardAvailable() {
		String root = null;
		Enumeration e = FileSystemRegistry.listRoots();
		while (e.hasMoreElements()) {
			root = (String) e.nextElement();
			if (root.equalsIgnoreCase("sdcard/")) {
				return true;
			}
		}
		return false;
	}

	public boolean isInternalStoreAvailable() {
		String root = null;
		Enumeration e = FileSystemRegistry.listRoots();
		while (e.hasMoreElements()) {
			root = (String) e.nextElement();
			if (root.equalsIgnoreCase("store/")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns the next id in sequence for a table. It's of course not thread 
	 * safe or anything like that. 
	 * @param tableName
	 * @return
	 */
	public int getNextId(String tableName) {
		incrementSequenceNumber(tableName, "id");
		String key = tableName + ".id";
		int value = ((Integer) nextSequenceNumbers.get(key)).intValue();
		Utils.log("[SQL] getNextId returned " + value + " for " + tableName);
		return value;
	}
	
	/**
	 * This is used to update our local cache for sequence numbers
	 * @param tableName
	 * @param columnName
	 */
	protected void incrementSequenceNumber(String tableName, String columnName){
		String key = tableName + "." + columnName;
		if(nextSequenceNumbers == null){
			nextSequenceNumbers = new Hashtable();
		}
		
		if(nextSequenceNumbers.containsKey(key)){
			int value = ((Integer) nextSequenceNumbers.get(key)).intValue();
			setSequenceNumber(tableName, columnName, value+1);
//			Utils.log("[SQL] Sequence number for " + tableName + "." + columnName + " updated to " + (value+1));
		} else {
//			Utils.log("[SQL] Couldn't find a key for: " + key);
			setSequenceNumber(tableName, columnName, 1);
		}
	}
	
	/**
	 * Sets the sequence number of a table. This is useful, if the table doesn't 
	 * even exist yet and we're busy handing out ids. 
	 * @param tableName
	 * @param columnName
	 * @param value
	 */
	protected void setSequenceNumber(String tableName, String columnName, int value){
		String key = tableName + "." + columnName;
		if(nextSequenceNumbers == null){
			nextSequenceNumbers = new Hashtable();
		}
		Utils.log("[SQL] Set the sequence number for " + tableName + "." + columnName + " to " + value);
		nextSequenceNumbers.put(key, new Integer(value));
	}
	

	/**
	 * Closes the connection to the database
	 */
	public boolean close() {
		if (database != null) {
			try {
				database.close();
				return true;
			} catch (DatabaseIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}


}
