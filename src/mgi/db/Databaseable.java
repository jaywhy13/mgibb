package mgi.db;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import mgi.db.schema.ForeignSchemaField;
import mgi.db.schema.SchemaDefinition;
import mgi.json.JSONArray;
import mgi.json.JSONException;
import mgi.json.JSONObject;
import mgi.util.Utils;

/**
 * This is an abstract class that implements the Databaseable interface
 * 
 * @author jay
 * 
 */
public abstract class Databaseable {
	
	public static final String FIELD_ID = "id";
	
	public Databaseable() {

	}
	
	/**
	 * Should we allow the if to change if the ID
	 * is already valid? 
	 */
	protected boolean allowValidIdChange = false;
	
	protected int id = -1;

	/**
	 * Returns true if the object supplied matches this object for all the
	 * variables the object has values for.
	 * 
	 * @param d
	 * @return
	 */
	public boolean like(Databaseable d) {
		Hashtable internalData = getData();
		Hashtable externalData = d.getData();

		Enumeration keys = externalData.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			String externalValue = externalData.get(key).toString();
			try {
				String internalValue = internalData.get(key).toString();
				if (!internalValue.equals(externalValue) && !internalValue.equals("-1") && !internalValue.equals("")) {
					return false;
				}
			} catch (NullPointerException npe) {
				npe.printStackTrace();
				return false;
			}
		}

		return true;
	}

	/**
	 * Inserts this type into the database
	 * 
	 * @return
	 */
	public boolean insert() {
//		Utils.log("Trying insert");
		if (DatabaseKeeper.isDatabaseReady()) {
			return DatabaseKeeper.getDatabase().insert(this);
		} else {
			Utils.warn("Keeper's not ready for us");
		}
		return false;
	}
	
	/**
	 * Prepares this data for insertion... in particular, it removes any -1. This is currently
	 * called by setId and by insert. 
	 * ids. 
	 */
	public void prepareDataForInsertion(DatabaseTemplate database){
//		Utils.log("[Databaseable] Preparing data for insertion");
		if(getId() == -1){
			// we need to update this
			Utils.log("about to get new auto id for: " + this);
			try {
				int newId = database.getNextId(getTableName());
				Utils.log("got new database id: " + newId);
				setId(newId, database); // update the id for this object
			} catch(NullPointerException e) {
				Utils.log("[:(] Could not fetch new id for object: " + this + ". The database is null");
			}
		}
	}
	
	/**
	 * Lets the child know that the id of the parent has been updated. 
	 * When this gets called, the child can update the related field. 
	 * @param d
	 */
	protected void parentUpdated(Databaseable d){
		Utils.warn("[Databaseable] parentUpdated IS NOT IMPLEMENTED on " + d.getTableName()); 
	}

	/**
	 * Insert into a specific database by name
	 * @param databaseName
	 * @return
	 */
	public boolean insert(String databaseName) {
		if (DatabaseKeeper.isDatabaseReady(databaseName)) {
			// Prepare this data for insertion...
			DatabaseTemplate db = (DatabaseTemplate) DatabaseKeeper.getDatabase(databaseName);
			prepareDataForInsertion(db);
			return db.insert(this);
		} else {
			Utils.warn("[Databaseable] Database: " + databaseName + " is not ready and cannot accomdate insertion of " + getTableName() + "\n" + toString());
		}
		return false;
	}

	/**
	 * Deletes this type from the database
	 * 
	 * @return
	 */
	public boolean delete() {
		if (DatabaseKeeper.isDatabaseReady()) {
			return DatabaseKeeper.getDatabase().delete(this);
		}
		return false;
	}

	public boolean delete(String databaseName) {
		if (DatabaseKeeper.isDatabaseReady(databaseName)) {
			return DatabaseKeeper.getDatabase(databaseName).delete(this);
		}
		return false;
	}

	/**
	 * A hash table of the values to be inserted into the database with their
	 * columns
	 * 
	 * @return
	 */
	public abstract Hashtable getData();

	public JSONObject toJSON() {
		Hashtable data = getData();
		JSONObject json = new JSONObject(data);
		Enumeration keys = data.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			Object obj = data.get(key);
			if (obj.getClass() == Vector.class) { // we need to replace this...
				json.remove(key);
				Vector databaseableItems = (Vector) obj;
				JSONArray jsonArr = new JSONArray();

				try {
					json.put(key, jsonArr);
				} catch (JSONException e) {
					Utils.warn("Failed to put " + key + " into the JSON Array: " + e.getMessage());
					e.printStackTrace();
				}

				for (int i = 0; i < databaseableItems.size(); i++) {
					Databaseable d = (Databaseable) databaseableItems.elementAt(i);
					JSONObject dAsJson = d.toJSON();
					jsonArr.put(dAsJson);
				}
			}
		}
		return json;
	}

	public boolean update() {
		if (DatabaseKeeper.isDatabaseReady()) {
			return DatabaseKeeper.getDatabase().update(this);
		}
		return false;
	}

	public boolean existsInDb() {
		return false;
	}

	public boolean read() {
		return false;
	}

	public boolean read(String databaseName) {
		return false;
	}

	/**
	 * This method converts a string of json text to a JSON Object
	 * 
	 * @param jsonStr
	 * @return
	 * @throws JSONException
	 */
	public JSONObject convertStringToJson(String jsonStr) throws JSONException {
		return new JSONObject(jsonStr);
	}

	/**
	 * Returns a new object given a Hashtable
	 * 
	 * @param data
	 * @return
	 */
	public abstract Databaseable fromHashtable(Hashtable data);
	
	public void updateData(Hashtable data){
		Utils.warn("[Databaseable] " + getTableName() + " UPDATE DATA method is NOT implemented");
	}

	/**
	 * Converts a JSON object into a new instance of this class.
	 * 
	 * @param object
	 * @return
	 */
	public abstract Databaseable fromJSON(JSONObject object) throws JSONException;

	public final Databaseable fromJSON(String jsonStr) {
		try {
			JSONObject obj = convertStringToJson(jsonStr);
			return fromJSON(obj);
		} catch (JSONException je) {
			Utils.err("There was an error in the JSON received! \n" + jsonStr);
			return null;
		}
	}

	/**
	 * Parses a result list of this type to get back a list of Databaseable
	 * objects
	 * 
	 * @param jsonStr
	 * @return
	 */
	public final Vector fromJSONList(String jsonStr) {
		Vector results = new Vector();
		try {
			JSONObject jsonObj = convertStringToJson(jsonStr);
			JSONArray list = Utils.getValue(jsonObj, "results", new JSONArray());
			for (int i = 0; i < list.length(); i++) {
				JSONObject obj = list.getJSONObject(i);
				Databaseable d = fromJSON(obj);
				results.addElement(d);
			}
		} catch (JSONException e) {
			Utils.err("Could not parse JSON: \n" + jsonStr);
		}
		return results;
	}

	/**
	 * Returns the name of the database table
	 * 
	 * @return
	 */
	public final String getTableName() {
		return "[" + this.getClass().getName() + "]";
	}

	public static Databaseable fromTableName(String tableName) {
		String className = Utils.replace("[", "", tableName);
		className = Utils.replace("]", "", className);
		return fromClassName(className);
	}

	public static Databaseable fromClassName(String className) {
		try {
			Class classs = Class.forName(className);
			Object newInstance = classs.newInstance();
			Databaseable d = (Databaseable) newInstance;
			return d;
		} catch (ClassNotFoundException e) {
			Utils.err("No class exists called " + className + ", cannot create Databaseable");
		} catch (InstantiationException e) {
			Utils.err("Error instantiating " + className);
		} catch (IllegalAccessException e) {
			Utils.err("Illegal access error instantiating " + className);
		}
		return null;
	}

	public static SchemaDefinition getSchemaForTableName(String tableName) {
		// first we need to strip the [ and ]
		String className = Utils.replace("[", "", tableName);
		className = Utils.replace("]", "", className);

		Databaseable d = fromClassName(className);
		if (d != null) {
			SchemaDefinition schema = d.getSchema();
			if (schema != null) {
				return schema;
			}
		}
		Utils.warn("We could not find a schema for: " + className);
		return null;
	}

	/**
	 * Returns the schema definition for the class
	 * 
	 * @return
	 */
	public abstract SchemaDefinition getSchema();

	public final int getId(){
		return this.id;
	}
	
	/**
	 * Sets the id of the object
	 * @param newId
	 */
	final public void setId(int newId){
		setId(newId, DatabaseKeeper.getDatabase());
	}

	/**
	 * Sets the id given the database. The database is used so children 
	 * can use the database can use the database tables to prepare for 
	 * insertion. That's basically just updating their ids. 
	 * @param id
	 * @param database
	 */
	final public void setId(int newId, DatabaseTemplate database){
		
		if(isIdValid() && !isAllowValidIdChange()){
			Utils.log("We cannot change the ID: " + getId() + " to " + newId + " for " + getTableName() + " because the ID is already valid");
			return;
		}
		
		if(getId() != newId && newId != -1){
			this.id = newId;
//			Utils.log("[Database] Updating ID " + getSchema().getTableName() + ".id = " + id);
			// Check if there are relationships we need to update
			if(getSchema() != null && getSchema().hasOneToManyFields()){
//				Utils.log("[Databaseable] " + getTableName() + ".id = " + id + " affects one to many fields, we will update these now");
				Vector oneToManyFields = getSchema().getOneToManyFields();
				for(int i = 0; i < oneToManyFields.size(); i++){
					Hashtable data = getData();
					ForeignSchemaField fkeyField = (ForeignSchemaField) oneToManyFields.elementAt(i);
//					Utils.log("[Databaseable] Checking for vector with name " + fkeyField.getRelatedName());
					Vector relatedItems = Utils.getValue(data, fkeyField.getRelatedName(), new Vector());
//					Utils.log("[Databaseable] " + relatedItems.size() + " " + fkeyField.getRelatedName() + " to update");
					for(int j = 0; j < relatedItems.size(); j++){
						Databaseable d = (Databaseable) relatedItems.elementAt(j);
						d.parentUpdated(this);
						d.prepareDataForInsertion(database);
					}
				}
			}
		}
	}

	public static Databaseable createFromData(String tableName, Hashtable data) {
		try {
			Databaseable d = (Databaseable) Class.forName(Utils.stripBraces(tableName)).newInstance();
			return d;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String [] getRequiredFields(){
		return new String [] {"id"};
	}
	
	public boolean validate(){
		boolean result = true;
		Hashtable data = getData();
		String [] requiredFieldNames = getRequiredFields();
		if(requiredFieldNames != null){
			for(int i = 0; i < requiredFieldNames.length; i++){
				String requiredFieldName = requiredFieldNames[i];
				Object obj = data.get(requiredFieldName);
				if(obj == null || obj.toString().equalsIgnoreCase("") || obj.toString() == "-1"){
					return false;
				}
			}
		}
		return result;
	}
	
	
	public String toString(){
		return toJSON().toString();
	}

	public boolean isAllowValidIdChange() {
		return allowValidIdChange;
	}

	public void setAllowValidIdChange(boolean allowValidIdChange) {
		this.allowValidIdChange = allowValidIdChange;
	}
	
	public boolean isIdValid(){
		return getId() > -1;
	}
	
	

}
