package mgi.db;

public interface Submittable {
	
	/**
	 * Submits something to the server
	 * @return
	 */
	public int submit();
	
	public boolean isSubmitted();
	
	public boolean isSubmitting();
	
	public void cancel();
	
	public int getTotal();
	
	public int getProgress();
	
}
