package mgi.db;

import java.util.Hashtable;
import java.util.Vector;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;

public class PersistentStoreDb extends DatabaseTemplate {

	private PersistentObject store;
	private Hashtable storeDatabase;

	private long storeId;

	public PersistentStoreDb(long storeId) {
		super();
		this.storeId = storeId;
	}

	public boolean createDatabase() {
		if (isDatabaseCreated()) {
			return true;
		}

		store = PersistentStore.getPersistentObject(storeId);
		storeDatabase = new Hashtable();
		store.setContents(storeDatabase);
		return store != null;
	}

	public boolean openDatabase() {
		if (!isDatabaseOpened()) {
			store = PersistentStore.getPersistentObject(storeId);
		}
		return store != null;
	}

	public boolean deleteDatabase() {
		if (openDatabase()) {
			store.setContents(null);
			store.commit();
			return false;
		}
		return false;
	}

	public boolean isDatabaseCreated() {
		store = PersistentStore.getPersistentObject(storeId);
		return store.getContents() == null;
	}

	public boolean isDatabaseOpened() {
		return store != null;
	}

	public boolean isDatabaseReady() {
		return isDatabaseOpened();
	}

	public boolean insert(Databaseable d) {
		if (isDatabaseReady()) {
			String key = InMemoryDatabase.getHashKey(d);
			storeDatabase.put(key, d);
			updateStore();
			return true;
		}
		return false;
	}

	public void updateStore() {
		if (openDatabase()) {
			store.setContents(storeDatabase);
			store.commit();
		}
	}

	public boolean update(Databaseable d) {
		return insert(d);
	}

	public boolean exists(Databaseable d) {
		if(isDatabaseReady()){
			return storeDatabase.containsKey(InMemoryDatabase.getHashKey(d));
		}
		return false;
	}

	public boolean exists(String tableName, Hashtable conditions) {
		return false;
	}

	public boolean exists(String tableName, int id) {
		return false;
	}

	public boolean exists(Databaseable d, int id) {
		return exists(d);
	}

	public Databaseable select(String tableName, int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Vector select(String tableName, Hashtable conditions) {
		// TODO Auto-generated method stub
		return null;
	}

	public Vector select(Databaseable d, Hashtable conditions) {
		// TODO Auto-generated method stub
		return null;
	}

	public int count(String tableName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int count(Databaseable d) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int count(String tableName, Hashtable conditions) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean delete(String tableName, int id) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(Databaseable d) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean beginTransaction() {
		return false;
	}

	public boolean commit() {
		return false;
	}

	public boolean rollback() {
		return false;
	}
	
	public boolean close() {
		return false;
	}

	public int max(Databaseable d) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int max(String tableName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int max(String tableName, String columnName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getNextId(String tableName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean delete(String tableName, Hashtable conditions) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
