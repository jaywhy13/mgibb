package mgi.db.schema;

import java.util.Vector;

public abstract class SchemaDefinition {
	
	public abstract Vector getFields();
	
	public abstract Vector getFields(boolean skipForeignKeys);
	
	public abstract void addField(SchemaField f);
	
	public abstract void addField(String fieldName, int fieldType);
	
	public abstract void addPrimaryKeyField(String fieldName);
	
	public abstract void addIntegerField(String fieldName);
	
	public abstract void addBooleanField(String fieldOpen);	
	
	public abstract void addTextField(String fieldName);
	
	public abstract SchemaField getFieldByName(String name);
	
	public abstract SchemaField getFieldByIndex(int index);
	
	public abstract String getTableName();
	
	public abstract void addForeignKey(String fieldName, String foreignTable, String relatedName);
	
	public abstract void addManyToOneField(String fieldName, String foreignTable, String relatedName);
	
	public abstract boolean hasForeignKeys();
	
	public abstract boolean hasPrimaryKey();
	
	public abstract SchemaField getPrimaryKey();
	
	public abstract Vector getManyToOneFields();
	
	public abstract Vector getOneToManyFields();
	
	public abstract Vector getForeignKeySql();
	
	/**
	 * Returns the schema definition as SQL
	 * @return
	 */
	public String asSQL(){
		String tableName = getTableName();
		
		String result = " CREATE TABLE " + tableName + " (";
		Vector nonKeyFields = getFields(true);
		for(int i = 0; i < nonKeyFields.size(); i++){
			SchemaField f = (SchemaField) nonKeyFields.elementAt(i);
			result += f.asSQL();
			if(i < nonKeyFields.size() - 1){
				result += ", ";
			}
		}

		Vector manyToOneKeys = getManyToOneFields();
		if(manyToOneKeys.size() > 0){
			result += ", ";
			for(int j = 0; j < manyToOneKeys.size(); j++){
				SchemaField f = (SchemaField) manyToOneKeys.elementAt(j);
				result += f.asSQL();
				if(j < manyToOneKeys.size() - 1){
					result += ", ";
				}
			}
		}
		
		result += " );";
		return result;
		
	}

	public Vector getForeignKeys() {
		Vector result = new Vector();
		for(int i = 0; i < getFields().size(); i++){
			SchemaField field = getFieldByIndex(i);
			if(field.isForeign()){
				result.addElement(field);
			}
		}
		return result;
	}
	
	public boolean hasOneToManyFields(){
		return getOneToManyFields().size() > 0;
	}

	
	
}
