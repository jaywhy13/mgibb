package mgi.db.schema;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import mgi.db.schema.SchemaDefinition;
import mgi.db.schema.SchemaManager;
import mgi.db.schema.SchemaManagerListener;




/**
 * The GLSchemaManager subclasses the SchemaManager. It holds schema definitions
 * for all the different types that need them.
 * 
 * @author JMWright
 * 
 */
public class GLSchemaManager extends SchemaManager {

	private boolean initialized = false;
	
	public Hashtable schemaDict = new Hashtable();

	private Vector listeners = new Vector();

	
	/**
	 * Adds a schema to the dictionary
	 */
	public void addSchema(String tableName, SchemaDefinition schema) {
		System.out.println("GL [II] Schema definition added for: " + tableName);
		schemaDict.put(tableName, schema);
	}

	public boolean hasSchema(String schemaName) {
		return schemaDict.get(schemaName) != null;
	}

	public SchemaDefinition getSchema(String schemaName) {
		if (hasSchema(schemaName)) {
			return (SchemaDefinition) schemaDict.get(schemaName);
		}
		return null;
	}
	
	public Vector getAllSchema() {
		Vector result = new Vector();
		Enumeration e = schemaDict.elements();
		while(e.hasMoreElements()){
			SchemaDefinition sd = (SchemaDefinition) e.nextElement();
			result.addElement(sd);
		}
		return result;
	}

	
	protected void defineSchema() {
		// Define ALL our schema
		// =======================================================================================
	}

	protected void _addSchemaManagerListener(SchemaManagerListener sml) {
		listeners.addElement(sml);
	}

	protected void _addSchemaManagerListeners(Vector v) {
		for (int i = 0; i < v.size(); i++) {
			SchemaManagerListener sml = (SchemaManagerListener) v.elementAt(i);
			_addSchemaManagerListener(sml);
		}
	}

	protected Vector getSchemaManagerListeners() {
		return listeners;
	}

	public boolean isInitialized() {
		return initialized;
	}
	
	protected void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

}
