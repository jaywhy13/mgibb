package mgi.db.schema;

import java.util.Vector;

import mgi.db.Databaseable;
import mgi.util.Utils;

public class ForeignSchemaField extends SchemaField {

	private String foreignTable;
	private String relatedName;
	private int mode = ONE_TO_MANY;

	public static int ONE_TO_MANY = 0;
	public static int MANY_TO_ONE = 1;
	public static int ONE_TO_ONE = 2;

	public ForeignSchemaField(String name, String foreignTable) {
		super(name, SchemaField.TYPE_INTEGER);
		this.foreignTable = foreignTable;
	}

	public ForeignSchemaField(String name, String foreignTable,
			String relatedName) {
		super(name, SchemaField.TYPE_INTEGER);
		this.foreignTable = foreignTable;
		this.relatedName = relatedName;
	}

	public boolean isPrimary() {
		return false;
	}

	public boolean isForeign() {
		return true;
	}

	public String getForeignTable() {
		return foreignTable;
	}

	public void setForeignTable(String foreignTable) {
		this.foreignTable = foreignTable;
	}

	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public boolean isManyToOne(){
		return mode == MANY_TO_ONE;
	}
	
	public boolean isOneToMany(){
		return mode == ONE_TO_MANY;
	}
	
	public boolean isOneTOne(){
		return mode == ONE_TO_ONE;
	}

	public String asSQL() {
		if (mode == ONE_TO_MANY) {
			// then we don't need to add a field on this relation
			return "";
		} else {
			String result = this.getName() + " "
					+ SchemaManager.getDatabaseTypeName(getType())
					+ (isPrimary() ? " PRIMARY KEY" : "");
			return result;

		}
	}

	public String getReferenceSql() {
		if(mode == MANY_TO_ONE){
			return " FOREIGN KEY (" + getName() + ") REFERENCES " + foreignTable + " (" + foreignTable + ".id)";
		}
		return "";
	}
	
	/**
	 * This method returns the name of the column this foreign key is 
	 * related to. 
	 * @return
	 */
	public String getColumnNameOnRelatedSchema(){
		SchemaDefinition foreignSchema = Databaseable.getSchemaForTableName(getForeignTable());
		if(foreignSchema != null){
			Vector relatedTableKeys = foreignSchema.getForeignKeys();
			String localTableName = getSchema().getTableName();
			
			if(relatedTableKeys.size() == 0){
				Utils.warn("[Database] " + foreignSchema.getTableName() + " has no related keys, cannot find which column matches with " + getSchema().getTableName() + "." + getName());
			}
			
			for(int i = 0; i < relatedTableKeys.size(); i++){
				ForeignSchemaField relatedTableField = (ForeignSchemaField) relatedTableKeys.elementAt(i);
				String foreignTable = relatedTableField.getForeignTable(); 
				if(foreignTable.equals(localTableName)){
					return relatedTableField.getName();
				}
			}
		} else {
			Utils.log("[Database] No schema available for field related to " + relatedName);
		}
		return null;
	}
	
	public String toString() {
		return getSchema().getTableName() + "." + getName();
	}

}
