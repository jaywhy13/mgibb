package mgi.db.schema;

import java.util.Vector;

public class StdSchemaDefinition extends SchemaDefinition {
	
	protected Vector fields = new Vector();

	private String tableName;
	
	private Vector fKeyDefs = new Vector();

	
	public StdSchemaDefinition(String tableName){
		this.tableName = tableName;
	}
	
	public void addField(SchemaField f) {
		f.setSchema(this);
		fields.addElement(f);
	}
	
	public Vector getFields(boolean skipForeignKeys){
		if(skipForeignKeys){
			Vector nonFKeyFields = new Vector();
			for(int i = 0; i < getFields().size(); i++){
				SchemaField field = (SchemaField) getFields().elementAt(i);
				if(!field.isForeign()){
					nonFKeyFields.addElement(field);
				}
			}
			return nonFKeyFields;
		} else {
			return getFields();
		}
	}

	public Vector getFields() {
		return fields;
	}

	public SchemaField getFieldByIndex(int index) {
		if(fields.size() > index){
			return (SchemaField) fields.elementAt(index);
		}
		return null;
	}

	public SchemaField getFieldByName(String name) {
		SchemaField result = null;
		for(int i = 0; i < fields.size(); i++){
			SchemaField f = getFieldByIndex(i);
			if(f.getName().equals(name)){
				result = f;
				break;
			}
		}
		return result;
	}

	public String getTableName() {
		return tableName;
	}

	public void setFields(Vector fields) {
		this.fields = fields;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public void addForeignKey(SchemaFKeyDefinition def) {
		fKeyDefs.addElement(def);
	}
	
	/**
	 * Returns true if the schema has a primary key defined 
	 */
	public boolean hasPrimaryKey() {
		boolean result = false;
		Vector fields = getFields();
		for(int i = 0; i < fields.size(); i++){
			SchemaField field = (SchemaField) fields.elementAt(i);
			if(field.isPrimary()){
				result = true;
				break;
			}
		}
		return result;
	}
	
	public SchemaField getPrimaryKey(){
		Vector fields = getFields();
		for(int i = 0; i < fields.size(); i++){
			SchemaField field = (SchemaField) fields.elementAt(i);
			if(field.isPrimary()){
				return field;
			}
		}
		return null;
	}

	public boolean hasForeignKeys() {
		for(int i = 0; i < fields.size(); i++){
			if(((SchemaField) fields.elementAt(i)).isForeign()){
				return true;
			}
		}
		return false;
	}

	public Vector getForeignKeySql() {
		Vector result = new Vector();

		if (hasForeignKeys()) {
			for (int i = 0; i < fields.size(); i++) {
				SchemaField field = (SchemaField) fields.elementAt(i);
				if (field.isForeign()) {
					ForeignSchemaField fKey = (ForeignSchemaField) field;
					if(fKey.getMode() == ForeignSchemaField.MANY_TO_ONE){
						result.addElement(fKey.getReferenceSql());
					}
				}
			}
		}
		return result;
	}

	public void addField(String fieldName, int fieldType) {
		SchemaField f = new SchemaField(fieldName, fieldType);
		addField(f);
	}

	public void addPrimaryKeyField(String fieldName) {
		SchemaField f = new SchemaField(fieldName, SchemaField.TYPE_INTEGER);
		f.setPrimary(true);
		addField(f);
	}

	public void addIntegerField(String fieldName) {
		SchemaField f = new SchemaField(fieldName, SchemaField.TYPE_INTEGER);
		addField(f);
	}

	public void addTextField(String fieldName) {
		SchemaField f = new SchemaField(fieldName, SchemaField.TYPE_TEXT);
		addField(f);
	}	

	public void addBooleanField(String fieldName) {
		SchemaField f = new SchemaField(fieldName, SchemaField.TYPE_BOOLEAN);
		addField(f);
	}

	public void addForeignKey(String fieldName, String foreignTable,
			String relatedName) {
		addField(new ForeignSchemaField(fieldName, foreignTable, relatedName));
	}
	
	public void addManyToOneField(String fieldName, String foreignTable, String relatedName){
		ForeignSchemaField fKey = new ForeignSchemaField(fieldName, foreignTable, relatedName);
		fKey.setMode(ForeignSchemaField.MANY_TO_ONE);
		addField(fKey);
	}

	public Vector getManyToOneFields() {
		Vector results = new Vector();
		if(hasForeignKeys()){
			Vector fKeys = getForeignKeys();
			for(int i = 0; i < fKeys.size(); i++){
				ForeignSchemaField fKey = (ForeignSchemaField) fKeys.elementAt(i);
				if(fKey.isManyToOne()){
					results.addElement(fKey);
				}
			}
		}
		return results;		
	}

	public Vector getOneToManyFields() {
		Vector results = new Vector();
		if(hasForeignKeys()){
			Vector fKeys = getForeignKeys();
			for(int i = 0; i < fKeys.size(); i++){
				ForeignSchemaField fKey = (ForeignSchemaField) fKeys.elementAt(i);
				if(fKey.isOneToMany()){
					results.addElement(fKey);
				}
			}
		}
		return results;	
	}

	
	
	

}
