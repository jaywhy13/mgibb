package mgi.maps;

import java.util.Hashtable;

import mgi.util.Utils;

public class Google {
	
	/**
	 * Get a static map url given the following parameters
	 * @param label
	 * @param color
	 * @param lat
	 * @param lon
	 * @param zoom
	 * @param width
	 * @param height
	 * @return
	 */
	public static String getStaticMapUrl(String label, String color, double lat, double lon, int zoom,
			int width, int height) {
		Hashtable data = new Hashtable();
		data.put("zoom", new Integer(zoom));
		data.put("format", "jpg");
		data.put("size", width + "x" + height);
		data.put("sensor", "false");
		String markerStr = "color:" + color + "|label:" + label + "|" + lat + "," + lon;
		data.put("markers", markerStr);
		String url = Utils.getUrl("http://maps.googleapis.com/maps/api/staticmap?",data);
		return url;
	}
	
	/**
	 * Get a default blue icon with label provided
	 * @param label
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static String getStaticMapUrl(String label, double lat, double lon){
		return getStaticMapUrl(label, "blue", lat, lon, 16, 400, 400);
	}
	
	
	public static String getStaticMapUrl(String label, double lat, double lon, int zoom, int width, int height){
		return getStaticMapUrl(label, "blue", lat, lon, zoom, width, height);
	}
	
	public static String getStaticMapUrl(String label, double lat, double lon, int zoom){
		return getStaticMapUrl(label, "blue", lat, lon, zoom, 400, 400);
	}
	
	public static String getStaticMapUrl(String label1, String label2, double lat1, double lon1, double lat2, double lon2, int width, int height){
		int zoom = 16;
		String color = "blue";
		Hashtable data = new Hashtable();
		data.put("size", width + "x" + height);
		data.put("format", "jpg");
		data.put("sensor", "false");
		
		String labelOrIcon1 = label1.indexOf("http:") == 0 ? "icon:" + label1 : "label:" + label1;
		String labelOrIcon2 = label2.indexOf("http:") == 0 ? "icon:" + label2 : "label:" + label2;
		
		String markerStr = "color:" + color + "|" + labelOrIcon1 + "|" + lat1 + "," + lon1;
		String markerStr2 = "color:" + color + "|label:" + labelOrIcon2 + "|" + lat2 + "," + lon2;
		String url = Utils.getUrl("http://maps.googleapis.com/maps/api/staticmap?",data);
		url += "&markers=" + markerStr + "&markers=" + markerStr2; 
		return url;
	}
	

	
	
	
}
