package mgi.tests;

import java.util.Vector;

import mgi.util.StoppableRunnable;
import mgi.util.Utils;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import de.enough.glaze.ui.component.ButtonField;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class TestScreen extends MainScreen
{
	
	LabelField statusMessage;
    /**
     * Creates a new MyScreen object
     */
    public TestScreen()
    {        
        // Set the displayed title of the screen       
        setTitle("Select a test to run");
        statusMessage = new LabelField(".......");
        add(statusMessage);
        Vector tests = Tests.getTests();
        for(int i = 0; i < tests.size(); i++){
        	final TestTemplate test = (TestTemplate) tests.elementAt(i);
        	ButtonField runTest = new ButtonField(test.getName());
        	runTest.setChangeListener(new FieldChangeListener() {
				
				public void fieldChanged(Field field, int context) {
					Utils.startThread(new StoppableRunnable(){
						public void run(){
							Tests.runTest(test.getName());
							setMessage("Running " + test.getName());
						}
					});
				}
			});
        	add(runTest);
        }
    }
    
    protected void setMessage(final String message){
    	Utils.invokeLater(new Runnable(){
    		public void run(){
    			statusMessage.setText(message);
    		}
    	});
    }
  
   
}
