package mgi.tests;

import net.rim.device.api.ui.UiApplication;

/**
 * This class extends the UiApplication class, providing a
 * graphical user interface.
 */
public class TestApp extends UiApplication
{


    /**
     * Creates a new MyApp object
     */
    public TestApp()
    {        
        // Push a screen onto the UI stack for rendering.
        pushScreen(new TestScreen());
    }    
}
