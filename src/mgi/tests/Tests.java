package mgi.tests;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import mgi.util.Utils;
import net.rim.device.api.ui.UiApplication;

public class Tests {
	private static final Hashtable tests = new Hashtable();

	private static boolean testRunning = false;
	
	/**
	 * Entry point for application
	 * 
	 * @param args
	 *            Command line arguments (not used)
	 */
	public static void debug() {
		// Create a new instance of the application and make the currently
		// running thread the application's event dispatch thread.
		TestApp theApp = new TestApp();
		theApp.enterEventDispatcher();
	}
	
	public static void debugInApp(){
		Utils.invokeLater(new Runnable(){
			public void run(){
				UiApplication.getUiApplication().pushScreen(new TestScreen());
			}
		});
	}

	public static void addTest(TestTemplate t) {
		tests.put(t.getName(), t);
	}
	
	public static void runTest(String testName){
		if(testRunning) {
			return;
		}
		testRunning = true;
		TestTemplate test = getTest(testName);
		if(test != null){
			test.test(); // run the test
		}
		showTestResults();
		testRunning = false;
	}
	
	public static void showTestResults(){
		Utils.invokeLater(new Runnable() {
			public void run() {
				UiApplication.getUiApplication().pushScreen(
						new UnitTestResultsScreen());
			}
		});
	}

	public static void runTests() {
		Thread testThread = new Thread(new Runnable() {
			public void run() {
				Enumeration keys = tests.keys();
				int total = tests.size();
				int test = 1;
				while (keys.hasMoreElements()) {
					String name = (String) keys.nextElement().toString();
					Utils.log("Running " + name + " (" + test + "/" + total + ")");
					TestTemplate t = (TestTemplate) tests.get(name);
					t.test();
					test++;
				}
				showTestResults();
			}
		});
		testThread.start();
	}

	public static Vector getTests() {
		Enumeration keys = tests.keys();
		Vector results = new Vector();
		while (keys.hasMoreElements()) {
			String name = (String) keys.nextElement().toString();
			TestTemplate t = (TestTemplate) tests.get(name);
			results.addElement(t);
		}
		return results;
	}
	
	public static TestTemplate getTest(String testName){
		return (TestTemplate) tests.get(testName);
	}


}
