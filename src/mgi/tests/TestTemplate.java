package mgi.tests;

import mgi.util.Utils;

public abstract class TestTemplate {

	public abstract int getNumTests();
	
	public abstract String getName();

	public abstract void test(int i) throws Throwable;

	public void test() {
		try {
			for (int i = 0; i < getNumTests(); i++) {
				test(i);
			}
		} catch (Throwable t) {
			Utils.debug("[TEST] Throwable exception occurred! " + t.getMessage());
			t.printStackTrace();
		}
	}

}
