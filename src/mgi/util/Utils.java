package mgi.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;

import me.regexp.RE;
import mgi.json.JSONArray;
import mgi.json.JSONException;
import mgi.json.JSONObject;
import mgi.net.MessageObject;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.blackberry.api.invoke.Invoke;
import net.rim.blackberry.api.invoke.PhoneArguments;
import net.rim.device.api.applicationcontrol.ApplicationPermissions;
import net.rim.device.api.applicationcontrol.ApplicationPermissionsManager;
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.WLANInfo;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontManager;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import de.enough.glaze.ui.component.LabelField;

public class Utils {

	private static boolean isDebug = false;

	private static Bitmap defaultAlertBitmap;

	private static Bitmap defaultAskBitmap;

	private static Hashtable errorCodes = new Hashtable();

	public static String getDeviceName() {
		return DeviceInfo.getDeviceName();
	}

	public static String getDeviceOS() {
		// return DeviceInfo.getPlatformVersion();
		return DeviceInfo.getSoftwareVersion();
	}

	public static void logLine() {
		System.out.println("===============================================");
	}

	public static void debug(String message) {
		System.out.println("[DEBUG] " + message);
	}

	public static void log(String message) {
		if (!isDebug) {
			System.out.println("[II] " + message);
		}
	}

	public static void err(String message) {
		System.out.println("[EE] " + message);
	}

	public static void warn(String message) {
		if (!isDebug) {
			System.out.println("[WW] " + message);

		}
	}

	public static void invokeLater(Runnable r) {
		UiApplication.getUiApplication().invokeLater(r);
	}

	public static void invokeAndWait(Runnable r) {
		UiApplication.getUiApplication().invokeAndWait(r);
	}

	public static String join(Vector strs, String delim) {
		String result = "";
		if (strs.size() == 1) {
			return strs.elementAt(0).toString();
		} else if (strs.size() == 0) {
			return result;
		} else {
			for (int i = 0; i < strs.size(); i++) {
				result += strs.elementAt(i).toString();
				if (i < strs.size() - 1) {
					result += delim;
				}
			}
		}
		return result;
	}

	public static String join(String[] str, String delim) {

		String result = "";
		if (str.length == 1) {
			return str[0];
		} else if (str.length == 0) {
			return result;
		} else {
			for (int i = 0; i < str.length; i++) {
				result += str[i];
				if (i < str.length - 1) {
					result += delim;
				}
			}
		}
		return result;
	}

	public static boolean requestApplicationPermissions(
			int[] requiredPermissions) {

		try {
			ApplicationPermissions currentPermissions = ApplicationPermissionsManager
					.getInstance().getApplicationPermissions();
			ApplicationPermissions missingPermissions = new ApplicationPermissions();

			// loop through and make sure we have all the permissions we need
			for (int i = 0; i < requiredPermissions.length; i++) {
				int requiredPermission = requiredPermissions[i];
				if (currentPermissions.getPermission(requiredPermission) != ApplicationPermissions.VALUE_ALLOW) {
					missingPermissions.addPermission(requiredPermission);
					Utils.warn("We are missing permission for: "
							+ requiredPermission);
				}
			}

			if (missingPermissions.getPermissionKeys().length > 0) {
				boolean permission = ApplicationPermissionsManager
						.getInstance().invokePermissionsRequest(
								missingPermissions);
				return permission;
			} else {
				return true;
			}
		} catch (Exception e) {
			err("Exception while setting permissions" + e);
		}
		return false;
	}

	public static String getValue(Hashtable data, String key,
			String defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				return data.get(key).toString();
			}
		}
		return defaultValue;
	}

	public static int getValue(JSONObject data, String key, int defaultValue) {
		if (data != null) {
			try {
				if (data.get(key) == null) {
					return defaultValue;
				} else {
					Object obj = data.get(key);
					Class classs = obj.getClass();
					if (obj.getClass() == Integer.class) {
						return ((Integer) data.get(key)).intValue();
					} else {
						return Integer.valueOf(obj.toString()).intValue();
					}

				}
			} catch (JSONException je) {

			}
		}
		return defaultValue;
	}

	public static double getValue(JSONObject data, String key,
			double defaultValue) {
		if (data != null) {
			try {
				if (data.get(key) == null) {
					return defaultValue;
				} else {
					Object obj = data.get(key);
					return Double.valueOf(obj.toString()).doubleValue();
				}
			} catch (JSONException je) {
				
			}
		}
		return defaultValue;
	}
	

	public static JSONArray getValue(JSONObject data, String key,
			JSONArray defaultValue) {
		if (data != null) {
			try {
				if (data.get(key) == null) {
					return defaultValue;
				} else {
					return data.getJSONArray(key);
				}
			} catch (JSONException je) {
				Utils.err("Could not get JSON Array with key: " + key
						+ " from the json");
			}
		}
		return defaultValue;
	}

	public static String getValue(JSONObject data, String key,
			String defaultValue) {
		if (data != null) {
			try {
				if (data.get(key) == null) {
					return defaultValue;
				} else {
					return data.get(key).toString();
				}
			} catch (JSONException je) {

			}
		}
		return defaultValue;
	}

	public static boolean getValue(JSONObject data, String key,
			boolean defaultValue) {
		if (data != null) {
			try {
				if (data.get(key) == null) {
					return defaultValue;
				} else {
					String str = data.get(key).toString();
					if (str.equalsIgnoreCase("true")
							|| str.equalsIgnoreCase("t")) {
						return true;
					} else {
						return false;
					}
				}
			} catch (JSONException je) {

			}
		}
		return defaultValue;
	}

	public static int getValue(Hashtable data, String key, int defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				Object obj = data.get(key);
				if (obj.toString().equals("")) {
					return defaultValue;
				}
				Class objClass = obj.getClass();
				Class integerClass = Integer.class;
				if (objClass == integerClass) {
					int result = ((Integer) obj).intValue();
					return result;
				} else if (objClass == String.class) {
					int result = Integer.parseInt(obj.toString());
					return result;
				}
			}
		}
		return defaultValue;
	}
	
	
	public static long getValue(Hashtable data, String key,
			long defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				Object obj = data.get(key);
				if (obj.toString().equals("")) {
					return defaultValue;
				}
				long result = Long.parseLong(obj.toString());
				return result;
			}
		}
		return defaultValue;
	}

	public static double getValue(Hashtable data, String key,
			double defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				Object obj = data.get(key);
				if (obj.toString().equals("")) {
					return defaultValue;
				}
				double result = Double.valueOf(obj.toString()).doubleValue();
				return result;
			}
		}
		return defaultValue;
	}

	public static boolean getValue(Hashtable data, String key,
			boolean defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				Object obj = data.get(key);
				if (obj.toString().equals("")) {
					return defaultValue;
				}

				Class objClass = obj.getClass();
				Class booleanClass = Boolean.class;
				if (objClass == booleanClass) {
					boolean result = ((Boolean) obj).booleanValue();
					return result;
				} else {
					// cast to string and check if it's equal to either 't' or
					// 'true'
					String str = obj.toString();
					if (str.equalsIgnoreCase("true")
							|| str.equalsIgnoreCase("t")) {
						return true;
					} else {
						return false;
					}
				}
			}
		}

		return defaultValue;

	}

	public static String stripBraces(String tableName) {
		tableName = tableName.trim();
		tableName = tableName.replace('[', ' ');
		tableName = tableName.replace(']', ' ');
		tableName = tableName.trim();
		return tableName;
	}

	/**
	 * Returns a debug string that shows general info about the device
	 * 
	 * @return
	 */
	public static String getDebugInfo() {
		String result = "";
		result += "Device Model: " + getDeviceName();
		result += "\nDevice OS: " + getDeviceOS();
		return result;
	}

	public static boolean isDebug(String[] args) {
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];
				if (arg.equals("debug")) {
					// isDebug = true;
					return true;
				}
			}
		}
		return false;
	}

	public static String getConnectionString() {

		String connectionString = null;

		// Simulator behaviour is controlled by the USE_MDS_IN_SIMULATOR
		// variable.
		if (DeviceInfo.isSimulator()) {

			connectionString = ";deviceside=true";
		}

		// Wifi is the preferred transmission method
		else if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {

			connectionString = ";interface=wifi";
		}

		// Is the carrier network the only way to connect?
		else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {

			String carrierUid = getCarrierBIBSUid();

			if (carrierUid == null) {
				// Has carrier coverage, but not BIBS. So use the carrier's TCP
				// network

				connectionString = ";deviceside=true";
			} else {
				// otherwise, use the Uid to construct a valid carrier BIBS
				// request

				connectionString = ";deviceside=false;connectionUID="
						+ carrierUid + ";ConnectionType=mds-public";
			}
		}

		// Check for an MDS connection instead (BlackBerry Enterprise Server)
		else if ((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {

			connectionString = ";deviceside=false";
		}

		// If there is no connection available abort to avoid hassling the user
		// unnecssarily.
		else if (CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE) {
			connectionString = "none";

		}

		// In theory, all bases are covered by now so this shouldn't be
		// reachable.But hey, just in case ...
		else {

			connectionString = ";deviceside=true";
		}

		return connectionString;
	}

	/**
	 * Looks through the phone's service book for a carrier provided BIBS
	 * network
	 * 
	 * @return The uid used to connect to that network.
	 */
	private synchronized static String getCarrierBIBSUid() {
		ServiceRecord[] records = ServiceBook.getSB().getRecords();
		int currentRecord;

		for (currentRecord = 0; currentRecord < records.length; currentRecord++) {
			if (records[currentRecord].getCid().toLowerCase().equals("ippp")) {
				if (records[currentRecord].getName().toLowerCase()
						.indexOf("bibs") >= 0) {
					return records[currentRecord].getUid();
				}
			}
		}

		return null;
	}

	public static JSONObject getValue(JSONArray moArr, int i,
			JSONObject defaultValue) {
		try {
			JSONObject obj = moArr.getJSONObject(i);
			return obj;
		} catch (JSONException e) {
			return defaultValue;
		}
	}

	public static void addError(int errorCode, String message) {
		errorCodes.put(new Integer(errorCode), message);
	}

	public static String getError(int errorCode) {
		if (errorCodes.containsKey(new Integer(errorCode))) {
			return errorCodes.get(new Integer(errorCode)).toString();
		}
		return "Unknown error: " + errorCode;
	}

	public static String getParamsAsString(Hashtable data) {
		return getParamsAsString(getParamsAsArray(data));
	}

	public static String getParamsAsString(String[] params) {
		return join(params, "&");
	}

	public static String getUrl(String url, Hashtable data) {
		if (data == null) {
			return url;
		}

		// Encode each url piece
		URLEncodedPostData ue = new URLEncodedPostData("UTF-8", false);
		Enumeration keys = data.keys();
		while (keys.hasMoreElements()) {
			String param = keys.nextElement().toString();
			String value = data.get(param).toString();
			ue.append(param, value);
		}

		String encodedParams = ue.toString();

		if (url.endsWith("?")) {
			return url + encodedParams;
		} else {
			return url + "?" + encodedParams;
		}
	}

	public static String[] getParamsAsArray(Hashtable data) {
		if (data == null) {
			return new String[] {};
		}
		String[] arr = new String[data.size()];
		int i = 0;
		Enumeration keys = data.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			if (data.get(key) != null) {
				arr[i] = key + "=" + data.get(key).toString();
			} else {
				arr[i] = key + "=";
			}
			i++;
		}
		return arr;
	}

	public static int getValue(MessageObject mo, String param, int defaultValue) {
		if (mo.hasParam(param)) {
			String value = mo.getParam(param);
			try {
				Integer integer = Integer.valueOf(value);
				return integer.intValue();
			} catch (ClassCastException cce) {
				return defaultValue;
			}
		} else {
			return defaultValue;
		}
	}

	public static String getValue(MessageObject mo, String param,
			String defaultValue) {
		if (mo.hasParam(param)) {
			String value = mo.getParam(param);
			return value;
		} else {
			return defaultValue;
		}
	}

	public static JSONObject getValue(MessageObject mo, String key,
			JSONObject defaultValue) {
		try {
			JSONObject obj = mo.getJson().getJSONObject(key);
			return obj;
		} catch (JSONException e) {
			return defaultValue;
		}
	}

	public static Vector getValue(Hashtable data, String key,
			Vector defaultValue) {
		if (data != null) {
			if (data.get(key) == null) {
				return defaultValue;
			} else {
				return (Vector) data.get(key);
			}
		}
		return defaultValue;
	}

	public static void put(Hashtable data, String key, Object value) {
		if (value != null) {
			data.put(key, value);
		}
	}

	public static void put(Hashtable data, String key, Object value,
			Object defaultValue) {
		if (value != null) {
			put(data, key, value);
		} else {
			put(data, key, defaultValue);
		}
	}

	public static String[] split(StringBuffer sb, String splitter) {
		String[] strs = new String[sb.length()];
		int splitterLength = splitter.length();
		int initialIndex = 0;
		int indexOfSplitter = indexOf(sb, splitter, initialIndex);
		int count = 0;
		if (-1 == indexOfSplitter)
			return new String[] { sb.toString() };
		while (-1 != indexOfSplitter) {
			char[] chars = new char[indexOfSplitter - initialIndex];
			sb.getChars(initialIndex, indexOfSplitter, chars, 0);
			initialIndex = indexOfSplitter + splitterLength;
			indexOfSplitter = indexOf(sb, splitter, indexOfSplitter + 1);
			strs[count] = new String(chars);
			count++;
		}
		// get the remaining chars.
		if (initialIndex + splitterLength <= sb.length()) {
			char[] chars = new char[sb.length() - initialIndex];
			sb.getChars(initialIndex, sb.length(), chars, 0);
			strs[count] = new String(chars);
			count++;
		}
		String[] result = new String[count];
		for (int i = 0; i < count; i++) {
			result[i] = strs[i];
		}
		return result;
	}

	public static int indexOf(StringBuffer sb, String str, int start) {
		int index = -1;
		if ((start >= sb.length() || start < -1) || str.length() <= 0)
			return index;
		char[] tofind = str.toCharArray();
		outer: for (; start < sb.length(); start++) {
			char c = sb.charAt(start);
			if (c == tofind[0]) {
				if (1 == tofind.length)
					return start;
				inner: for (int i = 1; i < tofind.length; i++) { // start on the
																	// 2nd
																	// character
					char find = tofind[i];
					int currentSourceIndex = start + i;
					if (currentSourceIndex < sb.length()) {
						char source = sb.charAt(start + i);
						if (find == source) {
							if (i == tofind.length - 1) {
								return start;
							}
							continue inner;
						} else {
							start++;
							continue outer;
						}
					} else {
						return -1;
					}

				}
			}
		}
		return index;
	}

	public static void alertError(int errorCode) {
		Dialog.inform(Utils.getError(errorCode));
	}

	public static String replace(String needle, String replacement,
			String haystack) {
		String result = "";
		int index = haystack.indexOf(needle);
		if (index == 0) {
			result = replacement + haystack.substring(needle.length());
			return replace(needle, replacement, result);
		} else if (index > 0) {
			result = haystack.substring(0, index) + replacement
					+ haystack.substring(index + needle.length());
			return replace(needle, replacement, result);
		} else {
			return haystack;
		}
	}

	public static void startThread(StoppableThread t) {
		t.start();
	}

	public static void startThread(final StoppableRunnable r) {
		new Thread(new Runnable() {
			public void run() {
				r.run();
			}
		}).start();
	}
	
	
	/**
	 * Returns the raw data for a file. Returns null is the file is not found 
	 * or if the path is null. If the file exists on the SD Card then, the path
	 * needs to be prefixed with /SDCard, e.g. /SDCard/BlackBerry/pictures/...
	 * @param path
	 * @return
	 */
	public static byte[] getFile(String path) {
		if (path == null) {
			throw new IllegalArgumentException("Cannot supply a null path");
		}

		if (!path.startsWith("file:")) {
			path = "file://" + path;
		}

		FileConnection fc;
		InputStream is;
		byte[] rawData = null;

		try {
			fc = (FileConnection) Connector.open(path, Connector.READ);
			if (fc.exists()) {
				is = fc.openInputStream();
				rawData = net.rim.device.api.io.IOUtilities.streamToBytes(is);
				fc.close();
			} else {
				Utils.warn(path + " does not exist! Cannot open");
			}
		} catch (IOException e) {
			Utils.debug("Exception occurred wihle trying to open " + path
					+ ". " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Utils.err(e.getMessage());
		}
		return rawData;
	}

	public Hashtable join(Hashtable h1, Hashtable h2) {
		Hashtable joinedHashTable = new Hashtable();
		Enumeration keys = h1.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			joinedHashTable.put(key, h1.get(key));
		}

		keys = h2.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement().toString();
			joinedHashTable.put(key, h2.get(key));
		}
		return joinedHashTable;
	}

	public static boolean loadFont(String fontLocation, String fontName) {
		try {
			InputStream stream = Utils.class.getClass().getResourceAsStream(
					fontLocation);
			FontManager.getInstance().load(stream, fontName,
					FontManager.APPLICATION_FONT);
			Utils.log("FONT: Loaded in " + fontName + " successfully");
			return true;
		} catch (Exception e) {
			Utils.err("FONT: Failed to load " + fontName + ". "
					+ e.getMessage());
			return false;
		}
	}

	/**
	 * Scales down an image
	 * 
	 * @param image
	 * @return
	 */
	public static byte[] scaleImage(byte[] image) {
		return scaleImage(image, 1.5);
	}

	/*
	 * Scales down an image by the given factor
	 */
	public static byte[] scaleImage(byte[] data, double scale) {
		EncodedImage image = EncodedImage.createEncodedImage(data, 0,
				data.length);

		int currentWidth32 = Fixed32.toFP(image.getWidth());
		int currentHeight32 = Fixed32.toFP(image.getHeight());

		int requiredWidth = (int) (image.getWidth() * scale);
		int requiredHeight = (int) (image.getHeight() * scale);

		int requiredWidth32 = Fixed32.toFP(requiredWidth);
		int requiredHeight32 = Fixed32.toFP(requiredHeight);

		int scaleXFixed32 = Fixed32.div(currentWidth32, requiredWidth32);
		int scaleYFixed32 = Fixed32.div(currentHeight32, requiredHeight32);

		EncodedImage scaledImage = image.scaleImage32(scaleXFixed32,
				scaleYFixed32);
		Utils.log("Image now has size: " + scaledImage.getWidth() + "x"
				+ scaledImage.getHeight());
		return scaledImage.getData();
	}

	public static void alert(final String msg) {
		Utils.invokeLater(new Runnable() {
			public void run() {
				Bitmap bitmap = getDefaultAlertBitmap();
				Dialog d = new Dialog(Dialog.D_OK, msg, Dialog.OK, bitmap,
						Dialog.DEFAULT_CLOSE);
				d.show();
			}
		});
	}

	public static int ask(final String msg) {
		Bitmap bitmap = getDefaultAlertBitmap();
		Dialog d = new Dialog(Dialog.D_YES_NO, msg, Dialog.YES, bitmap,
				Dialog.DEFAULT_CLOSE);
		return d.doModal();
	}

	public static String strip(String emailAddress) {
		if (emailAddress != null) {
			return emailAddress.trim();
		}
		return null;
	}

	public static void runAfter(final StoppableRunnable runnable,
			final long delay) {
		Utils.startThread(new StoppableRunnable() {
			public void run() {
				try {
					Thread.sleep(delay);
					runnable.run();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void runAfter(final Thread thread, final long delay) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(delay);
					thread.start();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

	public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/*
	 * public static boolean isValidEmailAddress(String stringToCheck) { boolean
	 * result = false; RE regex = new RE(EMAIL_REGEX); if
	 * (regex.match(stringToCheck)) { result = true; }
	 * 
	 * return result; }
	 */

	public static boolean isValidEmailAddress(String stringToCheck) {
		boolean result = false;
		RE regex = new RE(EMAIL_REGEX);
		if (regex.match(stringToCheck)) {
			result = true;
		}

		return result;
	}

	public static int getPercentage(int progress, int total) {
		if(total == 0){
			return 0;
		}
		
		int percentage = (int) (100 * ((double) progress / (double) total));
		return Math.min(percentage, 100);
	}

	public static String getPercentageAsString(int progress, int total) {
		int percentage = getPercentage(progress, total);
		String result = percentage + "%";
		return result;
	}

	public static boolean inArray(String needle, String[] haystack) {
		for (int i = 0; i < haystack.length; i++) {
			String potentialNeedle = haystack[i];
			if (potentialNeedle.equals(needle)) {
				return true;
			}
		}
		return false;
	}

	public static void setText(final LabelField labelField, final String txt) {
		Utils.invokeLater(new Runnable() {
			public void run() {
				labelField.setText(txt);
			}
		});
	}

	public static String shrinkToFit(Font font, String text, int maxWidth) {
		if (font != null) {
			if (font.getAdvance(text) > maxWidth) {
				while (font.getAdvance(text + "...  ") > maxWidth) {
					System.out.println("TEXT IS: "+ text);
					if (text.length() > 3) {
						text = text.substring(0, text.length() - 3);
					} else {
						break;
					}
				}

				// add ellipsis
				text = text + "...";
			}
		}
		return text;
	}

	public static String titleCase(String str) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			char chr = str.charAt(i);
			if (i == 0) {
				chr = Character.toUpperCase(chr);
			} else if (str.charAt(i - 1) == ' ') {
				chr = Character.toUpperCase(chr);
			}
			buf.append(chr);
		}
		return buf.toString();
	}

	public static void fillGradTopHalfRoundedRect(Graphics g, int x, int y,
			int width, int height, int arcRadius, int startColor,
			int finishColor, int alpha) {
		// Save some values
		int oldAlpha = g.getGlobalAlpha();
		int oldColor = g.getColor();
		int oldBgColor = g.getBackgroundColor();

		byte[] PATH_POINT_TYPES = {
				// Left mid point
				Graphics.CURVEDPATH_END_POINT,

				// Top left
				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT,

				// Top right
				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT,

				// Right mid
				Graphics.CURVEDPATH_END_POINT };

		int CURVE_X = arcRadius; // X-axis inset of curve
		int CURVE_Y = arcRadius;

		int[] PATH_GRADIENT = { finishColor, startColor, startColor,
				startColor, startColor, startColor, startColor, finishColor };

		int[] xPts = { x, // left mid
				x, x, CURVE_X, // top left
				width - CURVE_X, width, width, // top right
				width // right mid
		};
		int[] yPts = { height, // left
				CURVE_Y, y, y, // top left
				y, y, CURVE_Y, // top right
				height // right mid
		};

		// Draw a gradient on the top now...
		g.setGlobalAlpha(alpha);
		g.drawShadedFilledPath(xPts, yPts, PATH_POINT_TYPES, PATH_GRADIENT,
				null);

		g.setColor(oldColor);
		g.setGlobalAlpha(oldAlpha);
		g.setBackgroundColor(oldBgColor);
	}

	public static void fillGradRoundedRect(Graphics g, int x, int y, int width,
			int height, int arcRadius, int startColor, int finishColor,
			int alpha) {
		// Save some values
		int oldAlpha = g.getGlobalAlpha();
		int oldColor = g.getColor();
		int oldBgColor = g.getBackgroundColor();

		byte[] PATH_POINT_TYPES = { Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT,

				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT, Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT, };

		int CURVE_X = arcRadius; // X-axis inset of curve
		int CURVE_Y = arcRadius;

		int[] PATH_GRADIENT = { startColor, startColor, startColor, startColor,
				startColor, startColor,

				finishColor, finishColor, finishColor, finishColor,
				finishColor, finishColor };

		int[] xPts = { x, x, CURVE_X, width - CURVE_X, width, width, width,
				width, width - CURVE_X, CURVE_X, x, x };
		int[] yPts = { CURVE_Y, y, y, y, y, CURVE_Y, height - CURVE_Y, height,
				height, height, height, height - CURVE_Y };

		// Draw the bg first...
		g.setColor(Color.WHITE);
		g.fillRoundRect(0, 0, width, height, arcRadius * 2, arcRadius * 2);
		g.setColor(Color.BLACK);
		g.drawOutlinedPath(xPts, yPts, PATH_POINT_TYPES, null, true);

		// Draw a gradient on the top now...
		g.setGlobalAlpha(alpha);
		g.drawShadedFilledPath(xPts, yPts, PATH_POINT_TYPES, PATH_GRADIENT,
				null);

		g.setColor(oldColor);
		g.setGlobalAlpha(oldAlpha);
		g.setBackgroundColor(oldBgColor);
	}

	public static void fillGradLeftHalfRoundedRect(Graphics g, int x, int y,
			int width, int height, int arcRadius, int startColor,
			int finishColor, int alpha) {
		// Save some values
		int oldAlpha = g.getGlobalAlpha();
		int oldColor = g.getColor();
		int oldBgColor = g.getBackgroundColor();

		int CURVE_X = arcRadius; // X-axis inset of curve
		int CURVE_Y = arcRadius;

		byte[] PATH_POINT_TYPES = {
				// Top left corner
				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT,

				// Top right and bottom right
				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_END_POINT,

				// Bottom left corner
				Graphics.CURVEDPATH_END_POINT,
				Graphics.CURVEDPATH_QUADRATIC_BEZIER_CONTROL_POINT,
				Graphics.CURVEDPATH_END_POINT, };

		int[] PATH_GRADIENT = { startColor, startColor, startColor, startColor,
				finishColor, finishColor, finishColor, finishColor };

		int[] xPts = { x, x, CURVE_X, width - CURVE_X, width - CURVE_X,
				CURVE_X, x, x };
		int[] yPts = { CURVE_Y, y, y, y,

		y, height, height - CURVE_Y, height, height, height, height - CURVE_Y };

		g.setGlobalAlpha(alpha);
		g.drawShadedFilledPath(xPts, yPts, PATH_POINT_TYPES, PATH_GRADIENT,
				null);

		g.setGlobalAlpha(oldAlpha);
		g.setColor(oldColor);
		g.setBackgroundColor(oldBgColor);

	}

	public static Bitmap getDefaultAlertBitmap() {
		return defaultAlertBitmap;
	}

	public static void setDefaultAlertBitmap(Bitmap defaultAlertBitmap) {
		Utils.defaultAlertBitmap = defaultAlertBitmap;
	}

	public static Bitmap getDefaultAskBitmap() {
		return defaultAskBitmap;
	}

	public static void setDefaultAskBitmap(Bitmap defaultAskBitmap) {
		Utils.defaultAskBitmap = defaultAskBitmap;
	}

	public static String formatCurrency(double amount, String suffix) {
		String value = String.valueOf((int) amount);

		String finalAmount = "";
		for (int i = value.length() - 1; i >= 0; i--) {
			int indexFromEnd = value.length() - i;
			finalAmount = value.charAt(i) + finalAmount;
			if (i < value.length() - 1 && indexFromEnd % 3 == 0 && i > 0) {
				finalAmount = "," + finalAmount;
			}
		}
		Utils.log("Final amount for: " + amount + " is " + finalAmount);
		return finalAmount + (suffix == null ? "" : " " + suffix);
	}
	
	public static void describeServiceBooks(){
		ServiceBook serviceBook = ServiceBook.getSB();
		ServiceRecord [] records = serviceBook.getRecords();
		for(int i = 0; i < records.length; i++){
			ServiceRecord record = records[i];
			int id = record.getId();
			String cid = record.getCid();
			String apn = record.getAPN();
			String description = record.getDescription();
			String homeAddress = record.getHomeAddress();
			String name = record.getName();
			String networkAddress = record.getNetworkAddress();
			
		}
	}

	public static void makeCall(String phoneNumber) {
		if (phoneNumber == null || phoneNumber.length() == 0) {
			return;
		}

		PhoneArguments call = new PhoneArguments(PhoneArguments.ARG_CALL,
				phoneNumber);
		Invoke.invokeApplication(Invoke.APP_TYPE_PHONE, call);
	}

	public static String getAppVersion() {
		ApplicationDescriptor descriptor = ApplicationDescriptor
				.currentApplicationDescriptor();
		String version = descriptor.getVersion();
		return version;
	}

	public static void printHashTable(Hashtable data) {
		Enumeration keys = data.keys();
		log("[Hashtable contents]");
		while(keys.hasMoreElements()){
			String key = keys.nextElement().toString();
			Object value = data.get(key);
			String className = value == null ? "" : value.getClass().getName();
			if(value == null){
				value = "null";
			} else {
				value = value.toString();
			}
			log(" " + key + "=" + value);
		}
	}


	/**
	 * Returns the contents (raw byte data) of an image. If the file 
	 * is on the SD Card it needs to be prefixed with the /SDCard prefix. 
	 * For example, you can have /SDCard/BlackBerry/pictures...
	 * @param path
	 * @return
	 */
	public static Bitmap getImage(String path){
		byte [] data = getFile(path);
		if(data != null){
			EncodedImage image = EncodedImage.createEncodedImage(data, 0, data.length);
			if(image != null){
				return image.getBitmap();
			}
		}
		return null;
	}
	
	/**
	 * Returns the contents (raw byte data) of an image. If the file 
	 * is on the SD Card it needs to be prefixed with the /SDCard prefix. 
	 * For example, you can have /SDCard/BlackBerry/pictures...
	 * @param path
	 * @return
	 */
	public static EncodedImage getEncodedImage(String path){
		byte [] data = getFile(path);
		if(data != null){
			EncodedImage image = EncodedImage.createEncodedImage(data, 0, data.length);
			if(image != null){
				return image;
			}
		}
		return null;
	}
	
	public static boolean isSDCardInserted(){
		String sdCard = "file:///SDCard/";
		try {
			FileConnection fc = (FileConnection) Connector
			.open(sdCard);
			return fc.exists();
		} catch (IOException e) {
			return false;
		}
	}
	

}
