package mgi.util;

public interface Stoppable {
	public boolean isStopped();
	
	public void setStopped(boolean stopped);
}
