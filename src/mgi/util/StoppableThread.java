package mgi.util;

public class StoppableThread extends Thread implements Stoppable {
	
	private boolean stopped = false;

	public StoppableThread(Runnable runnable, String title) {
		super(runnable, title);
	}

	public boolean isStopped() {
		return stopped;
	}

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}
}
