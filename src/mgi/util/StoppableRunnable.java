package mgi.util;

public class StoppableRunnable implements Runnable, Stoppable {
	
	public void run() {
		
	}

	private boolean stopped = false;

	public boolean isStopped() {
		return stopped;
	}

	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}
		
}
