package mgi.util;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;

public class AppSettings {
	
	private static String settingsPrefix = "mgibbsetting_";
	
	public static void set(String key, Object value){
		String finalKey = settingsPrefix + key;
		PersistentObject store = PersistentStore.getPersistentObject(finalKey.hashCode());
		store.setContents(value);
		store.commit();
	}
	
	public static Object get(String key, Object defaultValue){
		String finalKey = settingsPrefix + key;
		PersistentObject store = PersistentStore.getPersistentObject(finalKey.hashCode());
		if(store.getContents() == null){
			return defaultValue;
		}
		return store.getContents();
	}
	
	public static Object get(String key){
		return get(key, null);
	}
	
	public static boolean hasSetting(String key){
		return get(key) != null;
	}

	public static String getSettingsPrefix() {
		return settingsPrefix;
	}

	public static void setSettingsPrefix(String settingsPrefix) {
		AppSettings.settingsPrefix = settingsPrefix;
	}
	
	
}
