package mgi.ui;

import java.util.Hashtable;
import java.util.Vector;

import mgi.events.GLPrerequisites;
import mgi.util.Utils;
import net.rim.device.api.ui.UiApplication;
import de.enough.glaze.ui.container.MainScreen;

public class Protector {

	private static Hashtable screenPrerequisites = new Hashtable();
	private static Hashtable failureScreens = new Hashtable();
	private static Vector failedScreens = new Vector();

	/**
	 * Adds a prerequiste for a screen
	 * 
	 * @param prerequisiteName
	 * @param screen
	 * @param failOverScreenName
	 */
	public static void addPrerequisite(String prerequisiteName,
			String screenClassName, String failOverScreenName) {
		Utils.log("Adding prerequisite " + prerequisiteName + " for "
				+ screenClassName);
		Vector prereqs = Utils.getValue(screenPrerequisites, screenClassName,
				new Vector());
		prereqs.addElement(prerequisiteName);
		screenPrerequisites.put(screenClassName, prereqs);
		failureScreens.put(screenClassName, failOverScreenName);
	}

	private static boolean prerequisitesSatisfied(MainScreen screen) {
		return prerequisitesSatisfied(screen.getClass().getName());
	}

	private static boolean prerequisitesSatisfied(String screenClassName) {
		Utils.log("Checking if prerequisite met for " + screenClassName);
		Vector prereqs = Utils.getValue(screenPrerequisites, screenClassName,
				new Vector());

		for (int i = 0; i < prereqs.size(); i++) {
			String prerequisiteName = prereqs.elementAt(i).toString();
			if (!GLPrerequisites.isPrerequisiteComplete(prerequisiteName)) {
				return false;
			}
		}
		return true;
	}

	public static boolean hasPrerequisites(MainScreen screen) {
		return hasPrerequisites(screen.getClass().getName());
	}

	public static boolean hasPrerequisites(String screenClassName) {
		return screenPrerequisites.get(screenClassName) != null;
	}
	
	public static String getPrerequisite(String screenClassName){
		if(hasPrerequisites(screenClassName)){
			return screenPrerequisites.get(screenClassName).toString();
		}
		return null;
	}
	
	public static boolean pushScreen(final MainScreen screen){
		return pushScreen(screen, null);
	}

	/**
	 * Returns true if the screen was pushed
	 * 
	 * @param screen
	 * @return
	 */
	public static boolean pushScreen(final MainScreen screen, Runnable logic) {
		String screenClassName = screen.getClass().getName();

		if (!hasPrerequisites(screen)) {
			Utils.warn(screenClassName + " has no prerequisites");
			UiApplication.getUiApplication().pushScreen(screen);
			if(logic != null){
				logic.run();
			}
			return true;
		}

		if (prerequisitesSatisfied(screen)) {
			UiApplication.getUiApplication().pushScreen(screen);
			if(logic != null){
				logic.run();
			}
			return true;
		} else {
			Utils.log(screenClassName
					+ " failed prerequisites, pushing failure screen");
			String failureScreenClassName = failureScreens.get(screenClassName)
					.toString();
			Object o = null;
			try {
				o = Class.forName(failureScreenClassName).newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (o != null) {
				final MainScreen failureScreen = (MainScreen) o;
//				failedLogic.put(getPrerequisite(screenClassName), logic);
				Utils.invokeLater(new Runnable() {
					public void run() {
						UiApplication.getUiApplication().pushScreen(
								failureScreen);
						
					}
				});
			}
		}
		// Note that this screen failed
		addScreenFailure(screen.getClass().getName());
		return false;
	}
	
	/**
	 * Adds the class name of the screen we failed to push unto the stack
	 * @param screenClassName
	 */
	private static void addScreenFailure(String screenClassName){
		failedScreens.addElement(screenClassName);
	}
	
	/**
	 * Clear all the screen failures for a screen 
	 * @param screenClassName
	 */
	public static void clearFailures(String screenClassName){
		failedScreens.removeElement(screenClassName);
	}

	
	public static void clearFailures(Object reportScreen) {
		clearFailures(reportScreen.getClass().toString());
	}

	/**
	 * Returns the name of the last failed screen. Null if none exists
	 * @return
	 */
	public static String getLastFailedScreen() {
		if(!failedScreens.isEmpty()){
			String screenClassName = failedScreens.elementAt(failedScreens.size()-1).toString();
			return screenClassName;
		}
		return null;
	}

}
